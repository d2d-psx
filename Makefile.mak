# the version of make in psyq is fucking OLD
# SEND HELP

OBJS=obj/a8.obj obj/bmap.obj obj/dots.obj obj/error.obj obj/files.obj \
obj/fx.obj obj/game.obj obj/items.obj obj/keyb.obj obj/main.obj obj/memory.obj obj/menu.obj \
obj/misc.obj obj/monster.obj obj/music.obj obj/utils.obj obj/player.obj obj/smoke.obj \
obj/sound.obj obj/switch.obj obj/vga.obj obj/view.obj obj/weapons.obj obj/memcard.obj

CFLAGS=-O3 -Dpsx -comments-c++
LDFLAGS=-Xo0x80010000 -llibmcrd

CPE=out/doom2d.cpe
EXE=out/doom2d.exe
SYM=out/doom2d.sym
MAP=out/doom2d.map
CD=doom2d.bin

$(CD): $(EXE)
	mkpsxiso -y doom2d.xml

$(EXE): $(CPE)
	cpe2x $(CPE)

# have to use a response file here because ccpsx
# can't take long command lines
$(CPE): $(OBJS)
	ccpsx $(LDFLAGS) @objs.link -o$(CPE),$(SYM),$(MAP)

obj/a8.obj: src/a8.c 
	ccpsx src/a8.c $(CFLAGS) -c -o obj/a8.obj 
obj/bmap.obj: src/bmap.c 
	ccpsx src/bmap.c $(CFLAGS) -c -o obj/bmap.obj 
obj/dots.obj: src/dots.c 
	ccpsx src/dots.c $(CFLAGS) -c -o obj/dots.obj 
obj/error.obj: src/error.c 
	ccpsx src/error.c $(CFLAGS) -c -o obj/error.obj 
obj/files.obj: src/files.c 
	ccpsx src/files.c $(CFLAGS) -c -o obj/files.obj 
obj/fx.obj: src/fx.c 
	ccpsx src/fx.c $(CFLAGS) -c -o obj/fx.obj 
obj/game.obj: src/game.c 
	ccpsx src/game.c $(CFLAGS) -c -o obj/game.obj 
obj/items.obj: src/items.c 
	ccpsx src/items.c $(CFLAGS) -c -o obj/items.obj 
obj/keyb.obj: src/keyb.c 
	ccpsx src/keyb.c $(CFLAGS) -c -o obj/keyb.obj 
obj/main.obj: src/main.c 
	ccpsx src/main.c $(CFLAGS) -c -o obj/main.obj 
obj/memory.obj: src/memory.c 
	ccpsx src/memory.c $(CFLAGS) -c -o obj/memory.obj 
obj/menu.obj: src/menu.c 
	ccpsx src/menu.c $(CFLAGS) -c -o obj/menu.obj 
obj/misc.obj: src/misc.c 
	ccpsx src/misc.c $(CFLAGS) -c -o obj/misc.obj 
obj/monster.obj: src/monster.c 
	ccpsx src/monster.c $(CFLAGS) -c -o obj/monster.obj 
obj/music.obj: src/music.c 
	ccpsx src/music.c $(CFLAGS) -c -o obj/music.obj 
obj/utils.obj: src/utils.c 
	ccpsx src/utils.c $(CFLAGS) -c -o obj/utils.obj 
obj/player.obj: src/player.c 
	ccpsx src/player.c $(CFLAGS) -c -o obj/player.obj 
obj/smoke.obj: src/smoke.c 
	ccpsx src/smoke.c $(CFLAGS) -c -o obj/smoke.obj 
obj/sound.obj: src/sound.c 
	ccpsx src/sound.c $(CFLAGS) -c -o obj/sound.obj 
obj/switch.obj: src/switch.c 
	ccpsx src/switch.c $(CFLAGS) -c -o obj/switch.obj 
obj/vga.obj: src/vga.c 
	ccpsx src/vga.c $(CFLAGS) -c -o obj/vga.obj 
obj/view.obj: src/view.c 
	ccpsx src/view.c $(CFLAGS) -c -o obj/view.obj 
obj/weapons.obj: src/weapons.c 
	ccpsx src/weapons.c $(CFLAGS) -c -o obj/weapons.obj 
obj/memcard.obj: src/memcard.c 
	ccpsx src/memcard.c $(CFLAGS) -c -o obj/memcard.obj 

clean:
	del obj\*.obj
	del out\*.exe
	del out\*.cpe
	del out\*.sym
	del out\*.map
	del out\*.img
	del *.TOC
