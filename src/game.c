/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "files.h"
#include "memory.h"
#include "vga.h"
#include "error.h"
#include "keyb.h"
#include "sound.h"
#include "view.h"
#include "bmap.h"
#include "fx.h"
#include "switch.h"
#include "weapons.h"
#include "items.h"
#include "dots.h"
#include "smoke.h"
#include "player.h"
#include "monster.h"
#include "menu.h"
#include "misc.h"
#include "map.h"
#include "memcard.h"

#define LT_DELAY   8
#define LT_HITTIME 6

#define GETIME     1092


int A8_start(char *);
int A8_nextframe(void);
void A8_close(void);


void FX_trans1(int t);

extern unsigned int lastkey;

extern int hit_xv, hit_yv;

extern int sw_secrets;

#define PL_FLASH 90

extern int PL_JUMP;

extern map_block_t blk;

byte cheat = 0;

byte _2pl = 0, g_dm = 0, g_st = GS_TITLE, g_exit = 0, g_map = 1, _warp = 1, g_music[8] = "MENU", g_loaded = 0;
byte _net  = 0;
int g_sttm = 1092;
dword g_time;
int dm_pnum, dm_pl1p, dm_pl2p;
pos_t dm_pos[100];

static void * telepsnd;

extern int sky_type;
void * ltn[2][2];
int lt_time, lt_type, lt_side, lt_ypos, lt_force;
void * ltnsnd[2];

int g_trans = 0, g_transt;

static void set_trans(int st) {
  switch (g_st) {
      case GS_ENDANIM:
      case GS_END2ANIM:
      case GS_DARKEN:
      case GS_BVIDEO:
      case GS_EVIDEO:
      case GS_END3ANIM:
        g_st = st;
        return;
  }
  switch (g_st = st) {
      case GS_ENDANIM:
      case GS_END2ANIM:
      case GS_DARKEN:
      case GS_BVIDEO:
      case GS_EVIDEO:
      case GS_END3ANIM:
        return;
  }
  g_trans  = 1;
  g_transt = 0;
}

int G_load(CDFILE *h) {
  switch (blk.t) {
      case MB_MUSIC:
        printf("G_load\n");
        cd_freadordie(g_music, 1, 8, h);
        return 1;
  }
  return 0;
}

void load_game(int n) {
  F_freemus();
  W_init();
  F_loadgame(n);
  set_trans(GS_GAME);
  // V_setscr((g_trans) ? fx_scr2 : scrbuf);
  V_setrect(0, SCRW, 0, SCRH); // V_setrect(0,320,0,200);
  // V_clr(0, SCRW, 0, SCRH, 0);  // V_clr(0,320,0,200,0);
  // V_setscr(scrbuf);
  pl1.drawst = 0xFF;
  if (_2pl)
    pl2.drawst = 0xFF;
  BM_remapfld();
  BM_clear(BM_PLR1 | BM_PLR2 | BM_MONSTER);
  BM_mark(&pl1.o, BM_PLR1);
  if (_2pl)
    BM_mark(&pl2.o, BM_PLR2);
  MN_mark();
  F_loadmus(g_music);
  S_startmusic(music_time);
}

void G_start(void) {
  char s[8];

  Z_loadingscreen(0);

  F_freemus();
  sprintf(s, "MAP%02u", (word) g_map);
  F_loadmap(s);
  printf("F_loadmap(%.8s)\n", s);
  set_trans(GS_GAME);
  // V_setscr((g_trans) ? fx_scr2 : scrbuf);
  V_setrect(0, SCRW, 0, SCRH); // V_setrect(0,320,0,200);
  // V_clr(0, SCRW, 0, SCRH, 0);  // V_clr(0,320,0,200,0);
  // V_setscr(scrbuf);
  pl1.drawst = 0xFF;
  if (_2pl)
    pl2.drawst = 0xFF;
  g_exit     = 0;
  itm_rtime  = (g_dm) ? 1092 : 0;
  p_immortal = 0;
  PL_JUMP    = 10;
  g_time     = 0;
  lt_time    = 1000;
  lt_force   = 1;
  BM_remapfld();
  BM_clear(BM_PLR1 | BM_PLR2 | BM_MONSTER);
  BM_mark(&pl1.o, BM_PLR1);
  if (_2pl)
    BM_mark(&pl2.o, BM_PLR2);
  MN_mark();
  F_loadmus(g_music);
  S_startmusic(music_time);
} /* G_start */

#define GGAS_TOTAL (MN__LAST - MN_DEMON + 16 + 10)

void G_init(void) {
  int i, j;
  char s[9];

  logo("G_init: loading game resources\n");

  telepsnd = Z_getsnd("TELEPT");
  for (i = 0; i < 2; ++i) {
    sprintf(s, "LTN%c", i + '1');
    for (j = 0; j < 2; ++j)
      ltn[i][j] = Z_getspr(s, j, 0, NULL);
  }
  ltnsnd[0] = Z_getsnd("THUND1");
  ltnsnd[1] = Z_getsnd("THUND2");
  DOT_alloc();
  SMK_alloc();
  FX_alloc();
  WP_alloc();
  IT_alloc();
  SW_alloc();
  PL_alloc();
  MN_alloc();
  Z_initst();

  logo("G_init: loading menu resources\n");

  W_setbg("TITLEPIC");

  pl1.color = 0;
  pl2.color = 5;
  g_trans   = 0;
} /* G_init */

int G_beg_video(void) {
  /*
   * switch(g_map) {
   *  case 3: return A8_start("FALL");
   *  case 4: return A8_start("KORIDOR");
   *  case 5: return A8_start("SKULL");
   *  case 6: return A8_start("TORCHES");
   *  case 7: return A8_start("CACO");
   *  case 8: return A8_start("DARTS");
   *  case 9: return A8_start("FISH");
   *  case 10: return A8_start("TRAP");
   *  case 11: return A8_start("JAIL");
   *  case 12: return A8_start("MMON1");
   *  case 13: return A8_start("TOWER");
   *  case 14: return A8_start("SAPOG");
   *  case 15: return A8_start("SWITCH");
   *  case 16: return A8_start("ACCEL");
   *  case 17: return A8_start("MEAT");
   *  case 18: return A8_start("LEGION");
   *  case 19: return A8_start("CLOUDS");
   * }
   */
  return 0;
}

int G_end_video(void) {
  /*
   * switch(g_map) {
   *  case 1: return A8_start("TRUBA");
   *  case 10: return A8_start("GOTCHA");
   * }
   */
  return 0;
}

static byte transdraw = 0;
extern void G_keyf(int x, int y);

void G_act(void) {
  static byte pcnt = 0;

  g_trans = 0;

  if (GM_act())
    return;

  switch (g_st) {
      case GS_TITLE:
      case GS_ENDSCR:
        return;

      case GS_INTER:
        if (K_held(K_P1_CROSS) || K_held(K_P1_START)) { // if (keys[SDLK_SPACE] || keys[SDLK_RETURN] || keys[SDLK_KP_ENTER])
          if (!G_beg_video()) {
            S_stopmusic();
            mc_dosave();
            S_startmusic(0);
            K_setkeyproc(G_keyf);
            G_start();
          } else {
            g_st = GS_BVIDEO;
            F_freemus();
          }
        }
        return;
  }

  if (sky_type == 2) {
    if ((lt_time > LT_DELAY) || lt_force) {
      if (!(rand() & 31) || lt_force) {
        lt_force = 0;
        lt_time  = -LT_HITTIME;
        lt_type  = rand() % 2;
        lt_side  = rand() & 1;
        lt_ypos  = rand() & 31;
        Z_sound(ltnsnd[rand() & 1], 64);
      }
    } else {
      ++lt_time;
    }
  }

  ++g_time;

  pl1.hit  = 0;
  pl1.hito = -3;
  if (_2pl) {
    pl2.hit  = 0;
    pl2.hito = -3;
  }

  W_act();
  IT_act();
  SW_act();
  if (_2pl) {
    if (pcnt) {
      PL_act(&pl1);
      PL_act(&pl2);
    } else {
      PL_act(&pl2);
      PL_act(&pl1);
    }
    pcnt ^= 1;
  } else {
    PL_act(&pl1);
  }
  MN_act();
  if (fld_need_remap)
    BM_remapfld();
  BM_clear(BM_PLR1 | BM_PLR2 | BM_MONSTER);
  BM_mark(&pl1.o, BM_PLR1);
  if (_2pl)
    BM_mark(&pl2.o, BM_PLR2);
  MN_mark();
  WP_act();
  DOT_act();
  SMK_act();
  FX_act();
  if (_2pl) {
    PL_damage(&pl1);
    PL_damage(&pl2);
    if (!(pl1.f & PLF_PNSND) && pl1.pain)
      PL_cry(&pl1);
    if (!(pl2.f & PLF_PNSND) && pl2.pain)
      PL_cry(&pl2);
    if ((pl1.pain -= 5) < 0) {
      pl1.pain = 0;
      pl1.f   &= (0xFFFF - PLF_PNSND);
    }
    if ((pl2.pain -= 5) < 0) {
      pl2.pain = 0;
      pl2.f   &= (0xFFFF - PLF_PNSND);
    }
  } else {
    PL_damage(&pl1);
    if (!(pl1.f & PLF_PNSND) && pl1.pain)
      PL_cry(&pl1);
    if ((pl1.pain -= 5) < 0) {
      pl1.pain = 0;
      pl1.f   &= (0xFFFF - PLF_PNSND);
    }
  }
  if (g_exit == 1) {
    if (G_end_video()) {
      F_freemus();
      g_st = GS_EVIDEO;
      return;
    }

inter:
    switch (g_map) {
        case 19:
          g_st = GS_ENDANIM;
          A8_start("FINAL");
          break;

        case 31:
        case 32:
          g_map = 16;
          set_trans(GS_INTER);
          break;

        default:
          ++g_map;
          set_trans(GS_INTER);
          break;
    }
    F_freemus();
    if (g_st == GS_INTER) {
      V_clearmem();
      W_setbg("INTERPIC");
      F_loadmus("INTERMUS");
    } else {
      V_clearmem();
      W_setbg("ENDPIC");
      F_loadmus("\x8a\x8e\x8d\x85\x96\x0");
      if (mus_vol > 0)
        S_volumemusic(128);
    }
    S_startmusic(0);
  } else if (g_exit == 2) {
    switch (g_map) {
        case 31:
          g_map = 32;
          set_trans(GS_INTER);
          break;

        case 32:
          g_map = 16;
          set_trans(GS_INTER);
          break;

        default:
          g_map = 31;
          set_trans(GS_INTER);
          break;
    }
    F_freemus();
    V_clearmem();
    W_setbg("INTERPIC");
    F_loadmus("INTERMUS");
    S_startmusic(0);
  }

  #ifdef DEMO
  if (g_dm && (g_time > 10920))
    set_trans(GS_INTER);

  #endif
} /* G_act */

/*
 * static void drawview(player_t *p) {
 * if(p->looky<-50) p->looky=-50;
 * else if(p->looky>50) p->looky=50;
 * w_x=p->o.x;w_y=p->o.y-12+p->looky;W_draw();PL_drawst(p);
 * }
 */
static inline int get_pu_st(int t) {
  if (t >= PL_FLASH)
    return 1;

  if ((t / 9) & 1)
    return 0;

  return 1;
}

static void drawview(player_t * p) {
  if (p->looky < -SCRH / 4)
    p->looky = -SCRH / 4;
  else if (p->looky > SCRH / 4)
    p->looky = SCRH / 4;
  w_x = p->o.x;
  w_y = p->o.y - 12 + p->looky;
  if (p->invl) override_pal = get_pu_st(p->invl) * 10;
  W_draw();
  if (p->invl) override_pal = 0;
  if (p->pain > 14)
    V_blendclr(0, SCRW-120, w_o, SCRH, 1, (p->pain - 14) << 1, 0, 0);
  PL_drawst(p);
}

static void pl_info(player_t * p, int y) {
  dword t;

  t = p->kills * 10920 / g_time;
  Z_gotoxy(25, y);
  Z_printbf("KILLS"); // Z_gotoxy(25,y);Z_printbf("KILLS");
  Z_gotoxy(25, y + 15);
  Z_printbf("KPM"); // Z_gotoxy(25,y+15);Z_printbf("KPM");
  Z_gotoxy(25, y + 30);
  Z_printbf("SECRETS %u OF %u", p->secrets, sw_secrets); // Z_gotoxy(25,y+30);Z_printbf("SECRETS %u / %u",p->secrets,sw_secrets);
  Z_gotoxy(255, y);
  Z_printbf("%u", p->kills); // Z_gotoxy(255,y);Z_printbf("%u",p->kills);
  Z_gotoxy(255, y + 15);
  Z_printbf("%u.%u", t / 10, t % 10); // Z_gotoxy(255,y+15);Z_printbf("%u.%u",t/10,t%10);
}

void G_draw(void) {
  int h;
  word hr, mn, sc;

  if (g_trans && !transdraw)
    return;

  switch (g_st) {
      case GS_ENDANIM:
      case GS_END2ANIM:
      case GS_DARKEN:
      case GS_BVIDEO:
      case GS_EVIDEO:
      case GS_END3ANIM:
        return;

      case GS_TITLE:
        V_center(1); //
        V_bigpic(0, 0, &bgimg);
        V_center(0); //
        break;

      case GS_ENDSCR:
        V_center(1); //
        V_clr(0, SCRW, 0, SCRH, 0);
        V_bigpic(0, 0, &bgimg); // V_clr(0,320,0,200,0);V_pic(0,0,scrnh[2]);
        V_center(0);           //
        break;

      case GS_INTER:
        V_center(1);                //
        V_clr(0, SCRW, 0, SCRH, 0); //
        V_bigpic(0, 0, &bgimg);
        Z_gotoxy(60, 20);
        Z_printbf("LEVEL COMPLETE");
        Z_calc_time(g_time, &hr, &mn, &sc);
        Z_gotoxy(115, 40);
        Z_printbf("TIME %u:%02u:%02u", hr, mn, sc);
        h = 60;
        if (_2pl) {
          Z_gotoxy(80, h);
          Z_printbf("PLAYER ONE");
          Z_gotoxy(80, h + 70);
          Z_printbf("PLAYER TWO");
          h += SCRH / 10; // h+=20;
        }
        pl_info(&pl1, h);
        if (_2pl)
          pl_info(&pl2, h + 70);
        V_center(0); //
        break;
  }
  V_center(1); //
  if (g_st != GS_GAME) {
    if (g_trans)
      return;

    GM_draw();
    V_copytoscr(0, SCRW, 0, SCRH); // V_copytoscr(0,320,0,200);
    return;
  }
  V_center(0); //

  if (_2pl) {
    HT  = SCRH / 2 - 2;
    w_o = 0;
    WD  = SCRW - 120;
    // limit to upper half of the screen
    V_setclip(0, SCRW, 0, HT);
    drawview(&pl1); // w_o=0;drawview(&pl1);
    w_o = SCRH / 2;
    WD  = SCRW - 120;
    // limit to lower half of the screen
    V_setclip(0, SCRW, w_o, HT);
    drawview(&pl2); // w_o=100;drawview(&pl2);
    V_setclip(0, SCRW, 0, SCRH); // reset clip
  } else {
    w_o = 0;
    WD  = SCRW - 120;
    HT  = SCRH - 2;
    drawview(&pl1); // w_o=50;drawview(&pl1);
  }

  if (g_trans)
    return;

  V_center(1); //
  if (GM_draw()) {
    pl1.drawst = pl2.drawst = 0xFF; // pl1.drawst=pl2.drawst=0;
    return;
  }
  V_center(0); //
  pl1.drawst = 0xFF;
  if (_2pl) pl2.drawst = 0xFF;
} /* G_draw */

void G_respawn_player(player_t * p) {
  int i;

  if (dm_pnum == 2) {
    if (p == &pl1)
      i = dm_pl1p ^= 1;
    else
      i = dm_pl2p ^= 1;
    p->o.x = dm_pos[i].x;
    p->o.y = dm_pos[i].y;
    p->d   = dm_pos[i].d;
    FX_tfog(dm_pos[i].x, dm_pos[i].y);
    Z_sound(telepsnd, 128);
    return;
  }
  do
    i = myrand(dm_pnum);
  while (i == dm_pl1p || i == dm_pl2p);
  p->o.x = dm_pos[i].x;
  p->o.y = dm_pos[i].y;
  p->d   = dm_pos[i].d;
  if (p == &pl1)
    dm_pl1p = i;
  else
    dm_pl2p = i;
  FX_tfog(dm_pos[i].x, dm_pos[i].y);
  Z_sound(telepsnd, 128);
}
