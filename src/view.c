/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include <string.h>
#include <malloc.h>
#include "vga.h"
#include "memory.h"
#include "files.h"
#include "error.h"
#include "view.h"
#include "dots.h"
#include "smoke.h"
#include "weapons.h"
#include "items.h"
#include "switch.h"
#include "fx.h"
#include "player.h"
#include "monster.h"
#include "misc.h"
#include "map.h"
#include "sound.h"

#define ANIT 5

/*
 #define WD 200
 #define HT 98
 *
 #define MAXX (FLDW*CELW-WD/2)
 #define MAXY (FLDH*CELH-HT/2)
 */

int WD; //
int HT; //

extern map_block_t blk;

void V_remap_rect(int, int, int, int, byte *);

byte w_drawsky = ON;
cacheimg bgimg;// static void *horiz=NULL;
vgaimg bghdr;
int w_o, w_x, w_y, sky_type = 1;
void * walp[256];
dword walf[256];
int walh[256];
byte walswp[256];
byte walani[256];
int anih[ANIT][5];
byte anic[ANIT];
byte fldb[FLDH][FLDW];
byte fldf[FLDH][FLDW];
byte fld[FLDH][FLDW];

extern int lt_time, lt_type, lt_side, lt_ypos;
extern void * ltn[2][2];

static void getname(int n, char * s) {
  if (walh[n] == -1) {
    memset(s, 0, 8);
    return;
  }
  if (walh[n] == -2) {
    memcpy(s, "_WATER_", 8);
    s[7] = (byte) walp[n] - 1 + '0';
    return;
  }
  F_getresname(s, walh[n] & 0x7FFF);
}

static short getani(char * n) {
  if (strncasecmp(n, "WALL22_1", 8) == 0)
    return 1;

  if (strncasecmp(n, "WALL58_1", 8) == 0)
    return 2;

  if (strncasecmp(n, "W73A_1", 8) == 0)
    return 3;

  if (strncasecmp(n, "RP2_1", 8) == 0)
    return 4;

  return 0;
}

void W_adjust(void) {
  int MAXX = (FLDW * CELW - WD / 2); //
  int MAXY = (FLDH * CELH - HT / 2); //

  if (w_x < WD / 2)
    w_x = WD / 2;
  if (w_y < HT / 2)
    w_y = HT / 2;
  if (w_x > MAXX)
    w_x = MAXX;
  if (w_y > MAXY)
    w_y = MAXY;
}

void W_draw(void) {
  W_adjust();
  V_setrect(0, WD, w_o + 1, HT);
  if (w_drawsky) {
    int y;
    for (y = w_o; y < w_o + HT; y += 256)
      V_pic(0, y, &bgimg);

    if (sky_type == 2) {
      if (lt_time < 0) {
        if (!lt_side)
          V_spr(0, w_o + lt_ypos, ltn[lt_type][(lt_time < -5) ? 0 : 1]);
        else
          V_spr2(WD - 1, w_o + lt_ypos, ltn[lt_type][(lt_time < -5) ? 0 : 1]);
      }
    }
  }
  Z_drawfld((byte *) fldb, 1);
  DOT_draw();
  IT_draw();
  PL_draw(&pl1);
  if (_2pl)
    PL_draw(&pl2);
  MN_draw();
  WP_draw();
  SMK_draw();
  FX_draw();
  Z_drawfld((byte *) fldf, 0);
  //if (sky_type == 2) {
  //  if ((lt_time == -4) || (lt_time == -2))
  //    V_remap_rect(0, WD, w_o + 1, HT, clrmap + 256 * 11);
  //}
} /* W_draw */

void W_init(void) {
  int i, j;
  static char * anm[ANIT - 1][5] = {
    { "WALL22_1", "WALL23_1", "WALL23_2", NULL,    NULL },
    { "WALL58_1", "WALL58_2", "WALL58_3", NULL,    NULL },
    { "W73A_1",   "W73A_2",   NULL,       NULL,    NULL },
    { "RP2_1",    "RP2_2",    "RP2_3",    "RP2_4", NULL }
  };

  for (i = 1; i < ANIT; ++i) {
    for (j = 0; anm[i - 1][j]; ++j)
      anih[i][j] = F_getresid(anm[i - 1][j]);
    for (; j < 5; ++j)
      anih[i][j] = -1;
  }
  memset(anic, 0, sizeof(anic));
  DOT_init();
  SMK_init();
  FX_init();
  WP_init();
  IT_init();
  SW_init();
  PL_init();
  MN_init();

  memset(&bgimg, 0, sizeof(bgimg));

  V_clearmem();
  S_clearmem();
}

void W_act(void) {
  int i, a;

  if (g_time % 3 != 0)
    return;

  for (i = 1; i < 256; ++i) {
    if ((a = walani[i]) != 0) {
      if (anih[a][++anic[a]] == -1)
        anic[a] = 0;
      walp[i] = V_cachepic(M_lock(anih[a][anic[a]]));
    }
  }
}

void unpack(void * buf, int len, void * obuf) {
  byte * p, * o;
  int l, n;

  for (p = buf, o = obuf, l = len; l; ++p, --l) {
    if (*p == 255) {
      p++;
      // n = *((word *) p); // for some reason this causes UNKNOWN OPCODE
      n = *(p++);
      n |= *(p++) << 8;
      memset(o, *p, n);
      o += n;
      l -= 3;
    } else {
      *(o++) = *p;
    }
  }
}

int W_load(CDFILE *h) {
  static char name[8];
  int i, j, k, g, num;
  void * p, * buf;
  wall_t *wbuf, *w;

  switch (blk.t) {
      case MB_WALLNAMES:
        for (i = 0; i < 256; ++i) {
          walh[i]   = -1;
          walswp[i] = i;
          walani[i] = 0;
          if (walp[i]) {
            if ((unsigned long)walp[i] > 3)
              V_uncachepic(walp[i]);
            walp[i] = NULL;
          }
        }

        num = blk.sz / sizeof(wall_t);
        wbuf = (wall_t *)malloc3(blk.sz);
        if (!wbuf) ERR_fatal("W_load(): out of memory\n");
        cd_freadordie(wbuf, 1, blk.sz, h);

        for (i = 1; i <= num; ++i) {
          w = wbuf + i - 1;
          if (w->n[0] == '_' && (w->n[1] == 'w' || w->n[1] == 'W')) { // if(memicmp(w.n,"_WATER_",7)==0)
            // there are no other textures with _W in the name, so who cares
            walp[i] = (void *) (w->n[7] - '0' + 1);
            walh[i] = -2;
            continue;
          }
          walp[i] = V_cachepic(M_lock(walh[i] = F_getresid(w->n)));
          if ((w->n[0] == 'S') && (w->n[1] == 'W') && (w->n[4] == '_'))
            walswp[i] = 0;
          walf[i] = (w->t) ? 1 : 0;
          if (w->t) walh[i] |= 0x8000;
          if (strncasecmp(w->n, "VTRAP01", 8) == 0)
            walf[i] |= 2;  // if(memicmp(w.n,"VTRAP01",8)==0) walf[i]|=2;
          walani[i] = getani(w->n);
        }

        for (j = i, i = 1; i < 256; ++i) {
          w = wbuf + i - 1;
          if (walswp[i] == 0) {
            if (j >= 256)
              break;
            w->n[5] ^= 1;
            g       = F_getresid(w->n) | (walh[i] & 0x8000);
            for (k = 1; k < 256; ++k) {
              if (walh[k] == g)
                break;
            }
            if (k >= 256) {
              walh[k = j++] = g;
              walp[k]       =  V_cachepic(M_lock(g));
              walf[k]       = (g & 0x8000) ? 1 : 0;
            }
            walswp[i] = k;
            walswp[k] = i;
          }
        }

        free3(wbuf);
        return 1;

      case MB_BACK:
        p = fldb;
        goto unp;

      case MB_WTYPE:
        p = fld;
        goto unp;

      case MB_FRONT:
        p = fldf;
unp:    switch (blk.st) {
            case 0:
              cd_freadordie(p, 1, FLDW * FLDH, h);
              break;

            case 1:
              if (!(buf = malloc3(blk.sz)))
                ERR_fatal("W_load(): out of memory\n");
              cd_freadordie(buf, 1, blk.sz, h);
              unpack(buf, blk.sz, p);
              free3(buf);
              break;

            default:
              return 0;
        }
        return 1;

      case MB_SKY:
        sky_type = 0;
        cd_freadordie(&sky_type, 1, 2, h);
        strcpy(name, "RSKY1");
        name[4] = sky_type + '0';
        if (w_drawsky) W_setbg(name);
        return 1;
  }
  return 0;
} /* W_load */

void W_setbg(char *name) {
  // HACK: using a separate struct here to keep backgrounds out of cache
  //       and still keep the size/whatever intact
  vgaimg *img = (vgaimg *)M_lock(F_getresid(name)); // load new bg
  memcpy(&bghdr, img, sizeof(bghdr)); // save the header
  bgimg.img = img;
  bgimg.tpage = 0xFFFF;
  V_fitpic(&bgimg); // upload into vram
  DrawSync(0); // wait until upload is complete
  bgimg.img = &bghdr; // substitute fake header
  M_unlock(img); // immediately free from main RAM
}
