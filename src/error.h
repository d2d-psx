/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

// Error handling

#include <stdlib.h>
#include <stdio.h>

extern char err_msg[256];

#define ERR_failinit(args...) do { sprintf(err_msg, args); ERR_panic(err_msg); } while(0);
#define ERR_fatal(args...) do { sprintf(err_msg, args); ERR_panic(err_msg); } while(0);

void ERR_panic(char *msg);
void ERR_close_all(void);
void ERR_quit(void);
