/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

// Globals

#ifndef MYGLOB
#define MYGLOB

#ifndef NULL
# define NULL 0
#endif

#define ON    1
#define OFF   0
#define TRUE  1
#define FALSE 0

typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned int dword;

#define logo(args...) printf(args)
void logo_gas(int, int);

extern int gammaa;
extern int snd_card;
extern int _cpu;

#define __MAX_PATH  255
#define __MAX_DRIVE 50
#define __MAX_DIR   100
#define __MAX_FNAME 50
#define __MAX_EXT   50

// define this to 1 if your DOOM2D.WAD was priority-sorted by splitwad.exe
// for nice sequential CD reads
#ifndef SORTED_WAD
# define SORTED_WAD 1
#endif

#ifndef offsetof
# define offsetof(st, m) ((unsigned long)&(((st *)0)->m))
#endif

#define bswap16(x) (((x) >> 8) | (((x) & 0xFF) << 8))
#define bswap32(x) ((bswap16((x) >> 16) & 0xFFFF) | (bswap16((x) & 0xFFFF) << 16))

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

#define myrand(a) (rand() % (a))

typedef struct CDFILE CDFILE;

CDFILE *cd_fopen(const char *fname);
void cd_fclose(CDFILE *f);
long cd_fread(void *ptr, long size, long num, CDFILE *f);
void cd_freadordie(void *ptr, long size, long num, CDFILE *f);
long cd_fseek(CDFILE *f, long ofs, int whence);
long cd_ftell(CDFILE *f);
long cd_fsize(CDFILE *f);
int cd_feof(CDFILE *f);

// don't actually need casecmp except in one instance
#define strcasecmp strcmp
#define strncasecmp strncmp

#define SCRW 320
#define SCRH 240

#ifndef REGION
# define REGION MODE_PAL
#endif

#if REGION == MODE_PAL
# define SECHBLANKS 15733
# define VBLANKTIME 315
# define FRAMETIME 787
# define SCRY 28
#else
# define SECHBLANKS 15625
# define VBLANKTIME 260
# define FRAMETIME 781
# define SCRY 0
#endif

#define FRAME_MSEC 50

#endif // ifndef MYGLOB
