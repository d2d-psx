#ifndef _DMEMCARD_H
#define _DMEMCARD_H

void mc_init(void);
void mc_done(void);
int mc_dosave(void);
int mc_doload(void);

void CFG_setdefaults(void);

#endif // _DMEMCARD_H
