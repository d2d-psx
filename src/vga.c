/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include "vga.h"
#include "error.h"
#include "view.h"

#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

#define NUMPALS 11 // 10 player colors + inverted bw

#define BORDER 96
#define PACKET_MAX 40000

#define PSXRGB(r, g, b) ((((b) >> 3) << 10) | (((g) >> 3) << 5) | ((r) >> 3))

static u_char loadimg[] = {
#include "loadpic.inc"
};

int override_pal = 0;
vgapal main_pal;
unsigned short clut_id[NUMPALS];
unsigned char clut_tab[NUMPALS] = {
  0x00, 0x18, 0x20, 0x40, 0x58,
  0x60, 0x80, 0xB0, 0xC0, 0xD0,
  0x00, // inverted bw palette
};

static int ofsx = 0;
static int ofsy = 0;

int cx1, cx2, cy1, cy2;

char fullscreen = OFF;

static struct drawbuf {
  DISPENV dispenv;
  DRAWENV drawenv;
  u_char gpudata[PACKET_MAX];
} dbuf[2];

struct drawbuf *curbuf = dbuf + 0;
static u_char *primptr, *lastprim;
static unsigned short curtpage = 0;
static RECT drawclip;
static int bufidx = 0;

static u_short wall_tpage = 0xFFFF;

#define MAX_CACHEDPICS 1024
#define CACHE_STARTX 320 // is already 64-aligned
#define CACHE_WIDTH  704 // 1024 - CACHE_STARTX
#define CACHE_HEIGHT 512

static cacheimg piccache[MAX_CACHEDPICS];
static u_short vram_alloc[CACHE_WIDTH];

short V_init(void) {
  int i, j;
  unsigned char *p;
  unsigned short *clut, *cp;

  ResetGraph(3);
  SetVideoMode(REGION);

  SetDefDispEnv(&dbuf[0].dispenv, 0, 0, SCRW, SCRH);
  SetDefDrawEnv(&dbuf[0].drawenv, 0, SCRH, SCRW, SCRH);
  SetDefDispEnv(&dbuf[1].dispenv, 0, SCRH, SCRW, SCRH);
  SetDefDrawEnv(&dbuf[1].drawenv, 0, 0, SCRW, SCRH);

  for (i = 0; i < 2; ++i) {
    dbuf[i].drawenv.isbg = 1;
    dbuf[i].drawenv.dtd = 0;
    dbuf[i].drawenv.dfe = 1;
    dbuf[i].dispenv.screen.w = 256; // in "display" coordinates
    dbuf[i].dispenv.screen.h = SCRH;
    dbuf[i].dispenv.screen.x = 0;
    dbuf[i].dispenv.screen.y = SCRY;
  }

  PutDispEnv(&dbuf[0].dispenv);
  PutDrawEnv(&dbuf[0].drawenv);

  // clear the splash leftovers and shit
  drawclip.w = 1024;
  drawclip.h = 512;
  ClearImage(&drawclip, 0, 0, 0);
  DrawSync(0);

  // upload cluts into vram

  p = &main_pal[0].r;
  cp = clut = malloc3(2 * 256);
  for (i = 0; i < 256; ++i, ++cp, p += 3) {
    *(p + 0) <<= 2;
    *(p + 1) <<= 2;
    *(p + 2) <<= 2;
    *cp = PSXRGB(*(p + 0), *(p + 1), *(p + 2));
    if (i && !*cp) *cp = 0x8000;
  }

  // base palette
  clut_id[0] = LoadClut((u_long*)clut, VRAM_PAL_X, VRAM_PAL_Y);

  // generate several variations for player colors and shit
  for (i = 1; i < NUMPALS - 1; ++i) {
    for (j = 0x70; j < 0x80; ++j)
      clut[j] = clut[j - 0x70 + clut_tab[i]];
    clut_id[i] = LoadClut((u_long*)clut, VRAM_PAL_X, VRAM_PAL_Y+i);
    DrawSync(0);
  }

  // generate inverted monochrome palette
  for (j = 0x01; j < 0xFF; ++j) {
    int c = (main_pal[j].r + main_pal[j].g + main_pal[j].b) / 3;
    c = 255 - c;
    clut[j] = PSXRGB(c, c, c);
    if (!clut[j]) clut[j] = 0x8000;
  }
  clut_id[NUMPALS - 1] = LoadClut((u_long*)clut, VRAM_PAL_X, VRAM_PAL_Y+NUMPALS-1);

  DrawSync(0);
  free3(clut);

  primptr = lastprim = NULL;

  SetDispMask(1);

  printf("V_init(): initialized video at %dx%dx16\n", SCRW, SCRH);

  return 0;
}

void V_done(void) {
  ResetGraph(0);
}

static inline void pushprim(u_long size) {
  catPrim(lastprim, primptr);
  lastprim = primptr;
  primptr += size;
}

void V_setclip(short x, short w, short y, short h) {
  drawclip.x = x;
  drawclip.y = y + !bufidx * SCRH;
  drawclip.w = w;
  drawclip.h = h;
  SetDrawArea((DR_AREA *)primptr, &drawclip);
  pushprim(sizeof(DR_AREA));
}

void V_setrect(short x, short w, short y, short h) {
  cx1 = x;
  cx2 = x + w - 1;
  cy1 = y;
  cy2 = y + h - 1;
  if (cx1 < 0)
    cx1 = 0;
  if (cx2 >= SCRW)
    cx2 = SCRW - 1;
  if (cy1 < 0)
    cy1 = 0;
  if (cy2 >= SCRH)
    cy2 = SCRH - 1;
}

void V_center(int f) {
  if (f)
    V_offset(SCRW / 2 - 320 / 2, SCRH / 2 - 200 / 2);
  else
    V_offset(0, 0);
}

void V_offset(int ox, int oy) {
  ofsx = ox;
  ofsy = oy;
}

static void draw_spr(short x, short y, cacheimg *i, int d, int c) {
  int w, h, sx, sy;
  POLY_FT4 *prim;

  if (!i || !i->img)
    return; // trying to draw nothing

  w = i->img->w;
  h = i->img->h;
  sx = i->img->sx;
  sy = i->img->sy;

  V_uploadpic(i); // attempt to upload right away so as to not cause slowdown later

  if (d & 1) x = x - w + sx + ofsx; else x = x - sx + ofsx;
  if (d & 2) y = y - h + sy + ofsy; else y = y - sy + ofsy;

  if (x + w < cx1 || x >= cx2 || y + h < cy1 || y >= cy2)
    return; // out of sight, out of mind

  prim = (POLY_FT4 *)primptr;
  setPolyFT4(prim);

  prim->tpage = i->tpage;
  prim->clut = override_pal ? clut_id[override_pal] : clut_id[c];
  setRGB0(prim, 128, 128, 128);

  prim->x0 = prim->x2 = x;
  prim->y0 = prim->y1 = y;
  prim->x1 = prim->x3 = x + w;
  prim->y2 = prim->y3 = y + h;

  if (w > 255) w = 255;
  if (h > 255) h = 255;

  // hflip
  if (d & 1) {
    prim->u1 = prim->u3 = i->u;
    prim->u0 = prim->u2 = i->u + w - 1;
  } else {
    prim->u0 = prim->u2 = i->u;
    prim->u1 = prim->u3 = i->u + w;
  }
  // vflip
  if (d & 2) {
    prim->v2 = prim->v3 = i->v;
    prim->v0 = prim->v1 = i->v + h - 1;
  } else {
    prim->v0 = prim->v1 = i->v;
    prim->v2 = prim->v3 = i->v + h;
  }

  pushprim(sizeof(POLY_FT4));

  wall_tpage = i->tpage; // don't want the tiles to use the sprite's tpage
}

// use this to draw textures more than 256 in width
// still limited to 256 height
void V_bigpic(int x, int y, cacheimg *i) {
  int xx, ww, tpage;
  int w = i->img->w;
  int h = i->img->h;
  POLY_FT4 *prim;

  V_uploadpic(i);

  x += ofsx;
  y += ofsy;

  tpage = i->tpage;

  // 1 tpage is normally 64 pixels wide, but our textures are 8bpp,
  // so it's basically 128 wide
  for (xx = 0; xx < w; xx += 256, tpage += 2) {
    prim = (POLY_FT4 *)primptr;
    setPolyFT4(prim);
    prim->tpage = tpage;
    prim->clut = clut_id[0];
    setRGB0(prim, 128, 128, 128);
    ww = w - xx;
    if (ww > 256) ww = 256;
    prim->x0 = prim->x2 = x + xx;
    prim->y0 = prim->y1 = y;
    prim->x1 = prim->x3 = x + xx + ww;
    prim->y2 = prim->y3 = y + h;
    prim->u0 = prim->u2 = i->u;
    prim->u1 = prim->u3 = i->u + ww - 1;
    prim->v0 = prim->v1 = i->v;
    prim->v2 = prim->v3 = i->v + h;
    pushprim(sizeof(POLY_FT4));
  }

  wall_tpage = tpage; // don't want the tiles to use the pic's tpage
}

void V_rotspr(int x, int y, cacheimg *i, int d) {
  x += i->img->w * ((d & 1) ? 1 : 0);
  y += i->img->h * ((d & 2) ? 1 : 0);
  draw_spr(x, y, i, d, 0);
}

void V_pic(short x, short y, cacheimg * i) {
  draw_spr(x, y, i, 0, 0);
}

void V_manspr(int x, int y, cacheimg * p, unsigned char c) {
  draw_spr(x, y, p, 0, c);
}

void V_manspr2(int x, int y, cacheimg * p, unsigned char c) {
  draw_spr(x, y, p, 1, c);
}

// вывести точку цвета c в координатах (x,y)
void V_dot(short x, short y, unsigned char c) {
  TILE_1 *prim;

  if (x < cx1 || x >= cx2 || y < cy1 || y >= cy2)
    return;

  prim = (TILE_1 *)primptr;

  setTile1(prim);
  prim->x0 = x;
  prim->y0 = y;
  prim->r0 = main_pal[c].r;
  prim->g0 = main_pal[c].g;
  prim->b0 = main_pal[c].b;

  pushprim(sizeof(TILE_1));
}

void smoke_sprf(int x, int y, byte c) {
}

void flame_sprf(int x, int y, byte c) {
}

void V_spr(short x, short y, cacheimg * i) {
  draw_spr(x, y, i, 0, 0);
}

void V_spr2(short x, short y, cacheimg * i) {
  draw_spr(x, y, i, 1, 0);
}

void V_clr(short x, short w, short y, short h, unsigned char c) {
  TILE *prim = (TILE *)primptr;

  setTile(prim);
  setXY0(prim, x, y);
  setWH(prim, w, h);
  prim->r0 = main_pal[c].r;
  prim->g0 = main_pal[c].g;
  prim->b0 = main_pal[c].b;

  pushprim(sizeof(TILE));
}

void V_transclr(short x, short w, short y, short h, unsigned char c) {
  TILE *prim = (TILE *)primptr;

  setTile(prim);
  setXY0(prim, x, y);
  setWH(prim, w, h);
  prim->r0 = main_pal[c].r;
  prim->g0 = main_pal[c].g;
  prim->b0 = main_pal[c].b;
  setSemiTrans(prim, 1);

  pushprim(sizeof(TILE));
}

void V_blendclr(short x, short w, short y, short h, short bl, unsigned char r, unsigned char g, unsigned char b) {
  TILE *prim;

  if (primptr + sizeof(TILE) + sizeof(DR_TPAGE) >= curbuf->gpudata + PACKET_MAX)
    return;

  setDrawTPage((DR_TPAGE *)primptr, 1, 0, getTPage(1, bl, 0, 0));
  pushprim(sizeof(DR_TPAGE));

  prim = (TILE *)primptr;

  setTile(prim);
  setXY0(prim, x, y);
  setWH(prim, w, h);
  prim->r0 = r;
  prim->g0 = g;
  prim->b0 = b;
  setSemiTrans(prim, 1);

  pushprim(sizeof(TILE));

  wall_tpage = 0xFFFF; // gotta invalidate this
}

// установить палитру из массива p
void VP_setall(void * p) {
  VP_set(p, 0, 256);
}

// установить n цветов, начиная с f, из массива p
void VP_set(void * p, short f, short n) {
}

// установить адрес экранного буфера
// NULL - реальный экран
void V_setscr(void * p) {
}

// скопировать прямоугольник на экран
void V_copytoscr(short x, short w, short y, short h) {
}

void V_maptoscr(int x, int w, int y, int h, void * cmap) {
}

void V_remap_rect(int x, int y, int w, int h, byte * cmap) {
}

extern void * walp[256];

static inline void draw_wall(cacheimg *pic, int sx, int sy) {
  int w = pic->img->w;
  int h = pic->img->h;

  V_uploadpic(pic);

  sx -= pic->img->sx;
  sy -= pic->img->sy;

  if (sx + w < cx1 || sx > cx2 || sy + h < cy1 || sy > cy2)
    return; // have to do this because some pics are big so the border is 96px

  if (wall_tpage != pic->tpage) {
    setDrawTPage((DR_TPAGE *)primptr, 1, 0, pic->tpage);
    pushprim(sizeof(DR_TPAGE));
    wall_tpage = pic->tpage;
  }

  if (w == 8 && h == 8) {
    // majority of tiles is 8x8, so I like to think this is justified
    SPRT_8 *prim = (SPRT_8 *)primptr;
    setSprt8(prim);
    setRGB0(prim, 128, 128, 128);
    prim->clut = clut_id[override_pal];
    prim->x0 = sx;
    prim->y0 = sy;
    prim->u0 = pic->u;
    prim->v0 = pic->v;
    pushprim(sizeof(SPRT_8));
  } else {
    SPRT *prim = (SPRT *)primptr;
    setSprt(prim);
    setRGB0(prim, 128, 128, 128);
    prim->clut = clut_id[override_pal];
    prim->x0 = sx;
    prim->y0 = sy;
    prim->u0 = pic->u;
    prim->v0 = pic->v;
    prim->w = w;
    prim->h = h;
    pushprim(sizeof(SPRT));
  }
}

void Z_drawfld(byte * fld, int bg) {
  static const wrgb[3][3] = { {0, 0, 255}, {0, 255, 0}, {255, 0, 0} };
  byte *p, *prow;
  int x, y, camx1, camy1, camx2, camy2;
  int sx, sy, hw, hh, i;
  cacheimg *pic;
  TILE_8 *prim;

  hw = WD >> 1;
  hh = (HT >> 1) + 1 + w_o;
  camx1 = (w_x - (WD >> 1)) >> 3;
  camx2 = camx1 + (WD >> 3) + 1;
  camy1 = (w_y - (HT >> 1)) >> 3;
  camy2 = camy1 + (HT >> 3) + 2;

  if ((camx1 -= 7) < 0) camx1 = 0;
  if ((camy1 -= 7) < 0) camy1 = 0;
  if (camx2 > FLDW) camx2 = FLDW;
  if (camy2 > FLDH) camy2 = FLDH;

  prow = fld + camy1 * FLDW + camx1;
  for (y = camy1; y < camy2; ++y, prow += FLDW) {
    p = prow;
    for (x = camx1; x < camx2; ++x, ++p) {
      sx = (x << 3) - w_x + hw;
      sy = (y << 3) - w_y + hh;
      if (*p) {
        pic = walp[*p];
        if ((u_long)pic <= 3) {
          if (sx + 8 < cx1 || sx > cx2 || sy + 8 < cy1 || sy > cy2)
            continue;
          i = (u_long)pic - 1;
          prim = (TILE_8 *)primptr;
          setTile8(prim);
          setXY0(prim, sx, sy);
          if (override_pal)
            setRGB0(prim, 0, 0, 0);
          else
            setRGB0(prim, wrgb[i][0], wrgb[i][1], wrgb[i][2]);
          setSemiTrans(prim, 1);
          pushprim(sizeof(TILE_8));
        } else {
          draw_wall(pic, sx, sy);
        }
      }
    }
  }
}

static void UnfuckLoadImage(RECT *r, vgaimg *img) {
  // resize and load image that has odd width
  // this shouldn't happen to any wallaby
  short i;
  short w = img->w;
  short h = img->h;
  short nw = w + 1;
  u_char *pout;
  u_char *pin = (u_char *)img + sizeof(vgaimg);
  u_char *buf = malloc3(nw * h);
  if (!buf) ERR_fatal("UnfuckLoadImage(): out of memory\n");
  for (pout = buf, i = 0; i < h; ++i, pin += w, pout += nw) {
    memcpy(pout, pin, w);
    *(pout + w) = 0; // fill extra column with transparency (or black)
  }
  LoadImage(r, (u_long *)buf);
  free3(buf);
}

void V_fitpic(cacheimg *cimg) {
  short besth = CACHE_HEIGHT;
  short i, th, j;
  u_short	vrx, vry;
  RECT r;

  if (!cimg)
    ERR_fatal("V_fitpic(): NULL image");

  // since our images are 8bpp, 2 pixels fit into one vram cell
  // so divide w by 2 rounding up, and also round h up to 2
  r.x = 0;
  r.y = 0;
  r.w = (cimg->img->w + 1) >> 1;
  r.h = cimg->img->h;

  if (r.w > 128)
    printf("V_fitpic(%dx%d): WARNING, > 256px width!\n", cimg->img->w, cimg->img->h);

  for (i = 0; i < CACHE_WIDTH - r.w; ++i) {
    th = 0;
    j = 0;
    while (j < r.w) {
      if (vram_alloc[i + j] >= besth)
          break;
      if (vram_alloc[i + j] > th)
          th = vram_alloc[i + j];
      ++j;
    }
    if (j == r.w) {
      r.x = i;
      besth = th;
      r.y = th;
    }
  }

  if (besth + r.h > CACHE_HEIGHT)
    ERR_fatal("V_fitpic(%dx%d): VRAM atlas full!\n", (int)cimg->img->w, (int)cimg->img->h);

  if (r.y < 256 && r.y + r.h > 256) {
    besth = 256; // crossing 256 pixel boundary, snap to the next 256
    r.y = 256;
  }

  for (i = 0; i < r.w; ++i)
    vram_alloc[r.x + i] = besth + r.h;

  r.x += CACHE_STARTX;

  if (cimg->img->w & 1)
    UnfuckLoadImage(&r, cimg->img);
  else
    LoadImage(&r, (u_long*)((u_char*)cimg->img + sizeof(vgaimg)));

  vrx = r.x & 0x3C0;
  vry = r.y & 0x100;
  cimg->tpage = getTPage(1, 0, vrx, vry);
  cimg->u = ((r.x - vrx) << 1) & 0xFF;
  cimg->v = r.y & 0xFF;
}

void V_clearmem(void) {
  int i;
  memset(vram_alloc, 0, sizeof(vram_alloc));
  for (i = 0; i < MAX_CACHEDPICS; ++i)
    piccache[i].tpage = 0xFFFF;
  wall_tpage = 0xFFFF;
}

cacheimg *V_cachepic(vgaimg *img) {
  int i = 0;
  if (!img) return NULL;
  for (i = 0; i < MAX_CACHEDPICS; ++i) {
    if (piccache[i].img == img)
      return piccache + i;
    if (!piccache[i].img) {
      piccache[i].img = img;
      piccache[i].tpage = 0xFFFF;
      return piccache + i;
    }
  }
  ERR_fatal("V_cachepic(): ran out of cache space!\n");
}

void V_uncachepic(cacheimg *cimg) {
  if (!cimg) return;
  M_unlock(cimg->img);
  cimg->img = NULL;
  cimg->tpage = 0xFFFF;
}

void V_startframe(void) {
  bufidx ^= 1;
  curbuf = dbuf + bufidx;
  lastprim = primptr = curbuf->gpudata;
  curtpage = 0;
}

void V_endframe(void) {
  termPrim(lastprim);
  DrawSync(0);
  VSync(0);
  PutDispEnv(&curbuf->dispenv);
  PutDrawEnv(&curbuf->drawenv);
  DrawOTag((u_long *)curbuf->gpudata);
}

void Z_loadingscreen(unsigned int x) {
  RECT r;
  // finish all rendering
  DrawSync(0);
  VSync(0);
  PutDispEnv(&curbuf->dispenv);
  PutDrawEnv(&curbuf->drawenv);
  // clear the screen
  // r.x = 0;
  r.y = SCRH * bufidx;
  // r.w = SCRW;
  // r.h = SCRH;
  // ClearImage(&r, 0, 0, 0);
  // and now just yeet the pic into framebuffer
  r.x = SCRW - 24;
  r.y += SCRH - 32;
  r.w = 15;
  r.h = 15;
  LoadImage2(&r, (u_long*)loadimg);
}

void Z_clearscreen(int rr, int g, int b) {
  RECT r;
  // finish all rendering
  DrawSync(0);
  VSync(0);
  PutDispEnv(&curbuf->dispenv);
  PutDrawEnv(&curbuf->drawenv);
  r.x = 0;
  r.y = 0;
  r.w = SCRW;
  r.h = SCRH*2;
  ClearImage(&r, rr, g, b);
}
