/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include "keyb.h"
#include "error.h"
#include <stdio.h>
#include <ctype.h>
#include <sys/file.h>
#include <sys/errno.h>
#include <libapi.h>
#include <libcd.h>

int strcasecmp(const char *s1, const char *s2) {
  register const unsigned char *p1 = (const unsigned char *) s1;
  register const unsigned char *p2 = (const unsigned char *) s2;
  register int result;
  if (p1 == p2)
    return 0;
  while ((result = tolower(*p1) - tolower(*p2++)) == 0)
    if (*p1++ == '\0')
      break;
  return result;
}

int strncasecmp(const char *s1, const char *s2, int n) {
  register const unsigned char *p1 = (const unsigned char *) s1;
  register const unsigned char *p2 = (const unsigned char *) s2;
  register int result;
  if (p1 == p2 || n == 0)
    return 0;
  while ((result = tolower(*p1) - tolower(*p2++)) == 0)
    if (*p1++ == '\0' || --n == 0)
      break;
  return result;
}

void mysplitpath(const char * path, char * drv, char * dir, char * name, char * ext) {
  const char * end; /* end of processed string */
  const char * p;   /* search pointer */
  const char * s;   /* copy pointer */

  /* extract drive name */
  if (path[0] && (path[1] == ':')) {
    if (drv) {
      *drv++ = *path++;
      *drv++ = *path++;
      *drv   = '\0';
    }
  } else if (drv) {
    *drv = '\0';
  }

  /* search for end of string or stream separator */
  for (end = path; *end && *end != ':';)
    end++;

  /* search for begin of file extension */
  for (p = end; p > path && *--p != '\\' && *p != '/';) {
    if (*p == '.') {
      end = p;
      break;
    }
  }

  if (ext) {
    for (s = end; (*ext = *s++);)
      ext++;
  }

  /* search for end of directory name */
  for (p = end; p > path;) {
    if ((*--p == '\\') || (*p == '/')) {
      p++;
      break;
    }
  }

  if (name) {
    for (s = p; s < end;)
      *name++ = *s++;

    *name = '\0';
  }

  if (dir) {
    for (s = path; s < p;)
      *dir++ = *s++;
    *dir = '\0';
  }
} /* mysplitpath */

// TEMPORARY CDFILE READING API WITH BUFFERS AND SHIT

static const u_long cdmode = CdlModeSpeed;

#define SECSIZE 2048
#define BUFSECS 4
#define BUFSIZE (BUFSECS * SECSIZE)

struct CDFILE {
  CdlFILE cdf;
  long secstart, secend, seccur;
  long fp, bufp;
  long bufleft;
  unsigned char buf[BUFSIZE];
};

CDFILE *cd_fopen(const char *fname) {
  CDFILE *f = calloc3(1, sizeof(CDFILE));
  if (!f) {
    ERR_fatal("cd_fopen(%s): out of memory\n", fname);
    return NULL;
  }

  // set hispeed mode
  CdControlB(CdlSetmode, (u_char*)&cdmode, 0);
  VSync(3); // have to do this to not explode the drive apparently

  if (CdSearchFile(&f->cdf, (char *)fname) == 0) {
    printf("cd_fopen(%s): file not found\n", fname);
    free3(f);
    return NULL;
  }

  // read first sector of the file
  CdControl(CdlSetloc, (u_char*)&f->cdf.pos, 0);
  CdRead(BUFSECS, (u_long*)f->buf, CdlModeSpeed);
  CdReadSync(0, 0);

  // set fp and shit
  f->secstart = CdPosToInt(&f->cdf.pos);
  f->seccur = f->secstart;
  f->secend = f->secstart + (f->cdf.size + SECSIZE-1) / SECSIZE;
  f->fp = 0;
  f->bufp = 0;
  f->bufleft = (f->cdf.size >= BUFSIZE) ? BUFSIZE : f->cdf.size;

  printf("cd_fopen(%s): size %u bufleft %d secs %d %d\n", fname, f->cdf.size, f->bufleft, f->secstart, f->secend);

  return f;
}

void cd_fclose(CDFILE *f) {
  if (!f) return;
  free3(f);
}

long cd_fread(void *ptr, long size, long num, CDFILE *f) {
  long rx, rdbuf;
  long fleft;
  CdlLOC pos;

  if (!f || !ptr) return -1;
  if (!size) return 0;

  size *= num;
  rx = 0;

  while (size) {
    // first empty the buffer
    rdbuf = (size > f->bufleft) ? f->bufleft : size;
    memcpy(ptr, f->buf + f->bufp, rdbuf);
    rx += rdbuf;
    ptr += rdbuf;
    f->fp += rdbuf;
    f->bufp += rdbuf;
    f->bufleft -= rdbuf;
    size -= rdbuf;

    // if we went over, load next sector
    if (f->bufleft == 0) {
      f->seccur += BUFSECS;
      // check if we have reached the end
      if (f->seccur >= f->secend)
        return rx;
      // looks like you need to seek every time when you use CdRead
      CdIntToPos(f->seccur, &pos);
      CdControl(CdlSetloc, (u_char*)&pos, 0);
      CdRead(BUFSECS, (u_long*)f->buf, CdlModeSpeed);
      CdReadSync(0, 0);
      fleft = f->cdf.size - f->fp;
      f->bufleft = (fleft >= BUFSIZE) ? BUFSIZE: fleft;
      f->bufp = 0;
    }
  }

  return rx;
}

void cd_freadordie(void *ptr, long size, long num, CDFILE *f) {
  if (cd_fread(ptr, size, num, f) < 0)
    ERR_fatal("cd_freadordie(%.16s): fucking died\n", f->cdf.name);
}

long cd_fseek(CDFILE *f, long ofs, int whence) {
  long fsec, bofs, left;
  CdlLOC pos;

  if (!f) return -1;

  if (whence == SEEK_CUR)
    ofs = f->fp + ofs;

  if (f->fp == ofs) return 0;

  fsec = f->secstart + (ofs / BUFSIZE) * BUFSECS;
  bofs = ofs % BUFSIZE;

  // fuck SEEK_END, it's only used to get file length here

  if (fsec != f->seccur) {
    // sector changed; seek to new one and buffer it
    CdIntToPos(fsec, &pos);
    CdControl(CdlSetloc, (u_char*)&pos, 0);
    CdRead(BUFSECS, (u_long*)f->buf, CdlModeSpeed);
    CdReadSync(0, 0);
    f->seccur = fsec;
    f->bufp = -1; // hack: see below
  }

  if (bofs != f->bufp) {
    // buffer offset changed (or new sector loaded); reset pointers
    left = f->cdf.size - ofs;
    f->bufp = bofs;
    f->bufleft = ((left >= BUFSIZE) ? BUFSIZE : left) - bofs;
    if (f->bufleft < 0) f->bufleft = 0;
  }

  f->fp = ofs;

  return 0;
}

long cd_ftell(CDFILE *f) {
  if (!f) return -1;
  return f->fp;
}

long cd_fsize(CDFILE *f) {
  if (!f) return -1;
  return f->cdf.size;
}

int cd_feof(CDFILE *f) {
  if (!f) return -1;
  return f->seccur >= f->secend;
}

void randomize(void) {
  srand(GetRCnt(0));
}
