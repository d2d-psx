/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include <stdio.h>
///#include <process.h>
#include <stdarg.h>
///#include <conio.h>
// #include <time.h>
///#include <dos.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "error.h"
#include "memory.h"
#include "keyb.h"
#include "sound.h"
#include "vga.h"
#include "files.h"
#include "view.h"
#include "menu.h"
#include "player.h"
#include "misc.h"
#include "memcard.h"

#include <kernel.h>

extern void randomize(void);

unsigned long __ramsize = 0x00200000; // 2mb RAM
unsigned long __stacksize = 0x00004000; // 16kb stack
extern unsigned long __heapbase;

static long ev_frame = -1;
static long ev_music = -1;

// HACK: this is an undocumented function that sets interrupt handlers for various shit
// index 6 is RCnt2, which we use to time music
// I have no fucking idea why it works fine but it does
extern void InterruptCallback(int index, void *cb);

int main(int argc, char * argv[]) {
  unsigned long heapsize = 0x80000000 + __ramsize - __stacksize - (u_long)__heapbase;

  pl1.id  = -1;
  pl2.id  = -2;

  ResetCallback();

  randomize();
  CFG_setdefaults();

  M_startup((void*)__heapbase, heapsize);
  F_startup();
  F_addwad("\\DOOM2D.WAD;1");

  F_initwads();
  F_allocres();
  F_loadres(F_getresid("PLAYPAL"), main_pal, 0, 768);

  // init video asap to display loading screens and shit
  logo("V_init: init video\n");
  if (V_init() != 0)
    ERR_failinit("could not init video");

  logo("S_init: init sound\n");
  S_init();
  S_initmusic();

  Z_loadingscreen(0);

  mc_init();

  G_init();
  K_init();

  GM_init();

  F_loadmus("MENU");
  S_startmusic(0);

  // add timer events for game frame and music frame
  EnterCriticalSection();
  ev_frame = OpenEvent(RCntCNT1, EvSpINT, EvMdNOINTR, NULL);
  ev_music = OpenEvent(RCntCNT2, EvSpINT, EvMdNOINTR, NULL);
  SetRCnt(RCntCNT1, FRAMETIME, RCntMdINTR);
  SetRCnt(RCntCNT2, DMM_FRAMETIME, RCntMdINTR);
  ResetRCnt(RCntCNT1);
  ResetRCnt(RCntCNT2);
  EnableEvent(ev_frame);
  EnableEvent(ev_music);
  StartRCnt(RCntCNT1);
  StartRCnt(RCntCNT2);
  InterruptCallback(6, S_updatemusic); // what the fuck
  ExitCriticalSection();

  if (ev_frame == -1 || ev_music == -1)
    ERR_fatal("could not set event timers (%d %d)\n", ev_frame, ev_music);

  printf("entering main loop\n");

  for (;;) {
    K_update();

    if (TestEvent(ev_frame)) {
      G_act();
      V_startframe();
      G_draw();
      V_endframe();
    }
  }
} /* main */
