/*
 * Драйвер клавиатуры V1.1 для DOS4GW (а также DirectX 3)
 * Copyright (C) Алексей Волынсков, 1996
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifndef MYKEYB
# define MYKEYBs

# ifdef __cplusplus
extern "C" {
# endif

#include <sys/types.h>
#include <libetc.h>

extern unsigned int pad_btns;
extern unsigned int pad_trig;

#define K_P1_UP       _PAD(0, PADLup)
#define K_P1_DOWN     _PAD(0, PADLdown)
#define K_P1_LEFT     _PAD(0, PADLleft)
#define K_P1_RIGHT    _PAD(0, PADLright)
#define K_P1_TRIANGLE _PAD(0, PADRup)
#define K_P1_CROSS    _PAD(0, PADRdown)
#define K_P1_SQUARE   _PAD(0, PADRleft)
#define K_P1_CIRCLE   _PAD(0, PADRright)
#define K_P1_L1       _PAD(0, PADL1)
#define K_P1_L2       _PAD(0, PADL2)
#define K_P1_R1       _PAD(0, PADR1)
#define K_P1_R2       _PAD(0, PADR2)
#define K_P1_START    _PAD(0, PADstart)
#define K_P1_SELECT   _PAD(0, PADselect)

#define K_P2_UP       _PAD(1, PADLup)
#define K_P2_DOWN     _PAD(1, PADLdown)
#define K_P2_LEFT     _PAD(1, PADLleft)
#define K_P2_RIGHT    _PAD(1, PADLright)
#define K_P2_TRIANGLE _PAD(1, PADRup)
#define K_P2_CROSS    _PAD(1, PADRdown)
#define K_P2_SQUARE   _PAD(1, PADRleft)
#define K_P2_CIRCLE   _PAD(1, PADRright)
#define K_P2_L1       _PAD(1, PADL1)
#define K_P2_L2       _PAD(1, PADL2)
#define K_P2_R1       _PAD(1, PADR1)
#define K_P2_R2       _PAD(1, PADR2)
#define K_P2_START    _PAD(1, PADstart)
#define K_P2_SELECT   _PAD(1, PADselect)

#define K_held(x) (pad_btns & (x))
#define K_pressed(x) (pad_trig & (x))

void K_init(void);
void K_done(void);
void K_update(void);
const char *K_name(int k);

// тип функции обработки клавиш
typedef void key_f (int k, int pressed);

// установить функцию обработки клавиш
void K_setkeyproc(key_f *);

# ifdef __cplusplus
}
# endif

#endif // ifndef MYKEYB
