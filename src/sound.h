/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifndef _SOUND_H
#define _SOUND_H

#ifdef __cplusplus
extern "C" {
#endif

#define SFX_CHANS 8
#define SFX_MASK 0xFFL
#define SFX_CH0 0

#define MUS_CHANS 8
#define MUS_MASK 0xFF00L
#define MUS_CH0 SFX_CHANS

#define DMM_FRAMETIME 65104 // in RCNT2 ticks

// we're using standard VAG files on the PSX
// these are actually big-endian, so we'll swap them later
#pragma pack(1)
typedef struct {
  char magic[4]; // VAGp, can actually be \0\0\0\0 sometimes (WHY)
  long version;
  long _unused1;
  long size;
  long rate;
  long _unused2;
  char name[16];
  // data follows
} snd_t;
#pragma pack()

typedef struct cachesnd {
  unsigned char *snd;
  short size;
  short rate;
  long spuaddr;
} cachesnd;

void S_init(void);

void S_done(void);

// проиграть звук s на канале c (1-8), частоте r и громкости v (0-255)
// возвращает номер канала, на котором играется звук
// если c==0, то звук попадет в любой свободный канал
// r - это относительная частота (обычно 1024)
short S_play(cachesnd * s, short c, unsigned r, short v);

// остановить звук на канале c (1-8)
void S_stop(short c);

void S_startmusic(int);
void S_stopmusic(void);
void S_updatemusic(void);
void S_sendmusicframe(void);
void S_intr(void);

// громкость звука и музыки (0-128)
extern short snd_vol, mus_vol;

void S_volumemusic(int v);
void S_volume(int v);
void S_wait(void);

void S_clearcache(void);
cachesnd *S_cachesound(snd_t *);
void S_unloadsound(cachesnd *snd);
void S_loadsound(cachesnd *snd);

extern char music_random;
extern int music_time;
extern int music_fade;

void F_freemus(void);
void S_initmusic(void);
void S_donemusic(void);

#ifdef __cplusplus
}
#endif

#endif // _SOUND_H
