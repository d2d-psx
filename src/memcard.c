#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <sys/types.h>
#include <libmcrd.h>

#include "glob.h"
#include "view.h"
#include "misc.h"
#include "vga.h"
#include "keyb.h"
#include "sound.h"
#include "player.h"

#define MAXSLOTS 4

#define CFGMAGIC "D2DC"
#define SAVMAGIC "D2DS"

#if REGION == MODE_PAL
# define SAVNAME "BED2DF-00666"
#else
# define SAVNAME "BAD2DF-00666"
#endif

#define SAVTITLE "Doom 2D"

#pragma pack(1)

// all memcard saves start with this

typedef struct {
  char id[2];        // "SC"
  char type;         // number of icon frames (0x11 - one frame, 0x12 - two frames, 0x13 - three frames)
  char size;         // size of save file in blocks
  u_short title[32]; // title of save file (encoded in Shift-JIS format)
  char pad[28];      // unused
  char clut[32];     // color palette of icon frames (16 RGB5X1 16-bit color entries)
} SAVEHDR;

// d2d saves are contained in 1 sector each
// sectors 0 and 1 contain the SAVEHDR, sector 2 is d2d_savcfg, sectors 3+ are d2d_savs

typedef struct {
  char id[4];      // "D2DC"
  u_char slotmask; // bit is 1 if a save exists in the slot
  char snd_vol;
  char mus_vol;
  char gamma;
  int p1keys[PLK_NUMKEYS];
  int p2keys[PLK_NUMKEYS];
} d2d_savcfg_t;

// following sectors are actual saves

typedef struct {
  u_char color;
  short life;
  short armor;
  short frag;
  short ammo;
  short shel;
  short rock;
  short cell;
  short fuel;
  char wpn;
  u_short wpns;
  u_char amul;
} d2d_savplr_t;

typedef struct {
  char id[4];  // "D2DS"
  char map[8]; // map resource name
  char numpl;  // 1 or 2
  char mode;   // 1 for dm
} d2d_sav_t;

#pragma pack(0)

static u_char secbuf[128];

// save icon

static u_short savicon_clut[16] = {
  0x8000, 0x1D93, 0x0011, 0x08A7,
  0x000C, 0x365C, 0x110B, 0x0464,
  0x0CEA, 0x1971, 0x0C7F, 0x42DF,
  0x25F9, 0x21D6, 0x112D, 0x3A9D
};

static u_char savicon[] = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x88,
  0x78, 0x00, 0x00, 0x00, 0x00, 0x30, 0x5D, 0xBB, 0x5B, 0x3D, 0x00, 0x00,
  0x00, 0x17, 0x55, 0xBB, 0x5B, 0x95, 0x07, 0x00, 0x00, 0x17, 0x5C, 0xFF,
  0x55, 0x1C, 0x07, 0x00, 0x00, 0x60, 0x99, 0xCD, 0x9D, 0x89, 0x07, 0x00,
  0x00, 0x30, 0x88, 0xE6, 0x8E, 0x38, 0x00, 0x00, 0x00, 0x77, 0x66, 0x86,
  0x66, 0x78, 0x07, 0x00, 0x00, 0x30, 0x24, 0x73, 0x23, 0x84, 0x00, 0x00,
  0x00, 0x40, 0xA2, 0x72, 0xA2, 0x42, 0x00, 0x00, 0x00, 0xE0, 0x38, 0x03,
  0x33, 0xE8, 0x00, 0x00, 0x00, 0x70, 0x33, 0x07, 0x33, 0x33, 0x00, 0x00,
  0x00, 0x70, 0x37, 0xEE, 0x3E, 0x07, 0x00, 0x00, 0x00, 0x00, 0x70, 0x07,
  0x77, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

// save names must be in SJIS, so here'a converter

static unsigned short ascii_table[3][2] = {
  {0x824f, 0x30},  /* 0-9  */
  {0x8260, 0x41},  /* A-Z  */
  {0x8281, 0x61},  /* a-z  */
};

static unsigned short ascii_k_table[] = {
  0x8140,    /*   */
  0x8149,    /* ! */
  0x8168,    /* " */
  0x8194,    /* # */
  0x8190,    /* $ */
  0x8193,    /* % */
  0x8195,    /* & */
  0x8166,    /* ' */
  0x8169,    /* ( */
  0x816a,    /* ) */
  0x8196,    /* * */
  0x817b,    /* + */
  0x8143,    /* , */
  0x817c,    /* - */
  0x8144,    /* . */
  0x815e,    /* / */
  0x8146,    /* : */
  0x8147,    /* ; */
  0x8171,    /* < */
  0x8181,    /* = */
  0x8172,    /* > */
  0x8148,    /* ? */
  0x8197,    /* @ */
  0x816d,    /* [ */
  0x818f,    /* \ */
  0x816e,    /* ] */
  0x814f,    /* ^ */
  0x8151,    /* _ */
  0x8165,    /* ` */
  0x816f,    /* { */
  0x8162,    /* | */
  0x8170,    /* } */
  0x8150,    /* ~ */
};

unsigned short ascii2sjis(unsigned char ascii_code) {
  int sjis_code = 0;
  unsigned char stmp = 0;
  unsigned char stmp2 = 0;

  if ((ascii_code >= 0x20) && (ascii_code <= 0x2f))
    stmp2 = 1;
  else if ((ascii_code >= 0x30) && (ascii_code <= 0x39))
    stmp = 0;
  else if ((ascii_code >= 0x3a) && (ascii_code <= 0x40))
    stmp2 = 11;
  else if ((ascii_code >= 0x41) && (ascii_code <= 0x5a))
    stmp = 1;
  else if ((ascii_code >= 0x5b) && (ascii_code <= 0x60))
    stmp2 = 37;
  else if ((ascii_code >= 0x61) && (ascii_code <= 0x7a))
    stmp = 2;
  else if ((ascii_code >= 0x7b) && (ascii_code <= 0x7e))
    stmp2 = 63;
  else {
    // printf("bad ASCII code 0x%x\n", ascii_code);
    return(0);
  }

  if (stmp2)
    sjis_code = ascii_k_table[ascii_code - 0x20 - (stmp2 - 1)];
  else
    sjis_code = ascii_table[stmp][0] + ascii_code - ascii_table[stmp][1];

  return sjis_code;
}

void str2sjis(char *asctext, u_short *sjistext) {
  int  i;
  char *ptr;
  union {
    u_short code;
    struct {
      char low;
      char high;
    } k_word;
  } kanji;
  for (i=0, ptr=(u_char*)sjistext; asctext[i] != 0x00; i++) {
    kanji.code	= ascii2sjis(asctext[i]);
    *ptr++ = kanji.k_word.high;
    *ptr++ = kanji.k_word.low;
  }
  sjistext[i] = 0x00;
}

// big-ass dialogs

#define HDRY ((SCRW >> 1) - 80)

static const char *mcnames[2] = {
  "MEMORY CARD 1",
  "MEMORY CARD 2",
};

static char slotnamebuf[MAXSLOTS][16] = {
  "EMPTY SLOT",
  "EMPTY SLOT",
  "EMPTY SLOT",
  "EMPTY SLOT",
};

static char *slotnames[MAXSLOTS] = {
  &slotnamebuf[0][0],
  &slotnamebuf[1][0],
  &slotnamebuf[2][0],
  &slotnamebuf[3][0],
};

static inline void scenterprint(int y, const char *str) {
  int len = Z_linelens((char*)str);
  Z_gotoxy((SCRW - len) >> 1, y);
  Z_prints((char *)str);
}

static inline void bcenterprint(int y, const char *str) {
  int len = Z_linelenb((char*)str);
  Z_gotoxy((SCRW - len) >> 1, y);
  Z_printb((char *)str);
}

static inline void waitscreen(const char *str1, const char *str2) {
  int i;
  for (i = 0; i < 2; ++i) {
    V_startframe();
    G_draw();
    V_transclr(0, SCRW, 0, SCRH, 247);
    bcenterprint(HDRY, str1);
    scenterprint(HDRY + 16, str2);
    V_endframe();
  }
}

static inline void msgscreen(const char *str1, const char *str2) {
  while (1) {
    K_update();
    waitscreen(str1, str2);
    if (K_pressed(K_P1_CROSS | K_P1_START))
      break;
  }
}

static inline int yesnoscreen(const char *msg) {
  while (1) {
    K_update();
    V_startframe();
    G_draw();
    V_transclr(0, SCRW, 0, SCRH, 247);
    scenterprint(90, msg);
    scenterprint(100, "(X - YES / O - NO)");
    V_endframe();
    if (K_pressed(K_P1_CROSS))
      return 1;
    if (K_pressed(K_P1_CIRCLE))
      return 0;
  }
  return 0;
}

static int menusel = 0;
static int menunum = 0;
static int menudone = 0;

static void selectmenu_keyf(int key, int st) {
  if (!st) return;
  switch (key) {
    case K_P1_UP:     if (menusel) --menusel; else menusel = menunum - 1; break;
    case K_P1_DOWN:   if (menusel < menunum - 1) ++menusel; else menusel = 0; break;
    case K_P1_CROSS:  menudone = 1; break;
    case K_P1_CIRCLE: menudone = -1;
  }
}

static inline int selectmenu(const char *hdr, int numopts, const char **opts) {
  int i, maxlen;

  if (menusel >= numopts || menusel < 0)
    menusel = 0;
  menunum = numopts;
  menudone = 0;

  K_setkeyproc(selectmenu_keyf);

  // align shit to the left, god damn it!
  maxlen = 0;
  for (i = 0; i < numopts; ++i) {
    int len = Z_linelens((char *)opts[i]);
    if (len > maxlen) maxlen = len;
  }

  while (!menudone) {
    K_update();
    V_startframe();
    G_draw();
    V_transclr(0, SCRW, 0, SCRH, 247);
    bcenterprint(HDRY, hdr);
    for (i = 0; i < numopts; ++i) {
      int y = HDRY + 16 + i * 8;
      Z_gotoxy((SCRW - maxlen) >> 1, y);
      Z_prints((char *)opts[i]);
      if (menusel == i) {
        Z_gotoxy(((SCRW - maxlen) >> 1) - 8, y);
        Z_putsfch('*');
        Z_gotoxy(((SCRW + maxlen) >> 1) + 2, y);
        Z_putsfch('*');
      }
    }
    V_endframe();
  }

  K_setkeyproc(NULL);

  return (menudone < 0) ? -1 : menusel;
}

// actual memcard shit

static u_char cards = 0;
static int curcard = 0;
static long mcresult[2];
static long mccmd;

void mc_init(void) {
  printf("mc_init(): initializing memcard system\n");
  MemCardInit(0);
}

void mc_done(void) {
  MemCardEnd();
}

static void mc_scan(void) {
  int i;
  waitscreen("PLEASE WAIT", "ENUMERATING MEMORY CARDS");

  cards = 0;

  for (i = 0; i < 2; ++i) {
    printf("mc_scan(): checking slot %d\n", i);
    MemCardExist(i * 16);
    while (!MemCardSync(1, &mccmd, mcresult + i));
    if (mcresult[i] == McErrNone || mcresult[i] == McErrNewCard) {
      printf("mc_scan(): card present\n");
      cards |= 1 << i;
    }
  }
}

static int mc_select(void) {
  int sel;

  switch (cards & 3) {
    case 3: sel = selectmenu("SELECT CARD", 2, mcnames); break;
    case 2: sel = selectmenu("SELECT CARD", 1, mcnames + 1); break;
    case 1: sel = selectmenu("SELECT CARD", 1, mcnames); break;
    default:
      msgscreen("ERROR", "NO MEMORY CARDS DETECTED");
      return -1;
  }

  if (sel < 0)
    return -1;
  else if ((cards & 3) == 2)
    ++sel;

  MemCardAccept(0x10 * sel);
  while (!MemCardSync(1, &mccmd, mcresult + sel));

  if (mcresult[sel] == McErrNotFormat) {
    int y = yesnoscreen("UNFORMATTED CARD. FORMAT?");
    if (!y) {
      msgscreen("ERROR", "CANNOT CONTINUE WITH UNFORMATTED CARD");
      return -1;
    }

    waitscreen("PLEASE WAIT", "FORMATTING CARD");

    MemCardFormat(0x10 * sel);

    MemCardAccept(0x10 * sel);
    while (!MemCardSync(1, &mccmd, mcresult + sel));
  }

  return sel;
}

extern byte g_map, g_dm, _2pl;
static d2d_savcfg_t cfg;
static d2d_sav_t sav;
static d2d_savplr_t savplr[2];
static SAVEHDR savhdr;

static inline int selectslot(int slotmask) {
  int i;
  for (i = 0; i < MAXSLOTS; ++i)
    if (cfg.slotmask & (1 << i))
      sprintf(slotnamebuf[i], "SAVE %d", i);
    else
      strcpy(slotnamebuf[i],  "EMPTY SLOT");
  return selectmenu("SELECT SAVE", MAXSLOTS, (const char **)slotnames);
}

int mc_doload(void) {
  int mc, j, i, slot, err;
  int res = 0;

  MemCardStart();

  mc_scan();
  mc = mc_select();
  if (mc < 0) goto _end2;

  waitscreen("PLEASE WAIT", "CHECKING GAME DATA");

  memset(secbuf, 0, sizeof(secbuf));
  memset(&cfg, 0, sizeof(cfg));
  memset(&sav, 0, sizeof(sav));
  memset(&savhdr, 0, sizeof(savhdr));
  memset(savplr, 0, sizeof(savplr));

  // open save
  if ((err = MemCardOpen(0x10 * mc, SAVNAME, O_RDONLY)) != McErrNone) {
    msgscreen("ERROR", "YOU HAVE NO SAVE FILE ON THIS CARD");
    goto _end2;
  }

  MemCardReadData((long *)secbuf, 128 * 2, 128);
  while (!MemCardSync(1, &mccmd, mcresult + mc));
  memcpy(&cfg, secbuf, sizeof(cfg));
  if (mcresult[mc] != McErrNone || memcmp(cfg.id, CFGMAGIC, 4)) {
    msgscreen("ERROR", "INVALID SAVE FILE");
    goto _end1;
  }

  // read from the config
  S_volume(cfg.snd_vol);
  S_volumemusic(cfg.mus_vol);
  for (i = 0; i < PLK_NUMKEYS; ++i) {
    *(int *)((u_char *)&pl1 + plk_ofs[i]) = cfg.p1keys[i];
    *(int *)((u_char *)&pl2 + plk_ofs[i]) = cfg.p2keys[i];
  }

  if (cfg.slotmask == 0) {
    msgscreen("ERROR", "YOU HAVE NO SAVED GAMES");
    goto _end1;
  }

  // get the slot
  do {
    slot = selectslot(cfg.slotmask);
    if (slot < 0) goto _end1;
  } while (!(cfg.slotmask & (1 << slot)));

  waitscreen("PLEASE WAIT", "LOADING GAME");

  // load that shit
  MemCardReadData((long *)secbuf, 128 * (3 + slot), 128);
  while (!MemCardSync(1, &mccmd, mcresult + mc));
  memcpy(&sav, secbuf, sizeof(sav));
  if (mcresult[mc] != McErrNone || memcmp(sav.id, SAVMAGIC, 4)) {
    msgscreen("ERROR", "INVALID SAVE FILE");
    goto _end1;
  }

  memcpy(savplr, secbuf + sizeof(sav), sizeof(savplr));

  // set map and mode
  _2pl = sav.numpl - 1;
  g_dm = sav.mode;
  g_map = 10 * (sav.map[3] - '0') + (sav.map[4] - '0');

  // initialize players
  pl1.color = savplr[0].color;
  pl1.life = savplr[0].life;
  pl1.armor = savplr[0].armor;
  pl1.frag = savplr[0].frag;
  pl1.ammo = savplr[0].ammo;
  pl1.shel = savplr[0].shel;
  pl1.rock = savplr[0].rock;
  pl1.cell = savplr[0].cell;
  pl1.fuel = savplr[0].fuel;
  pl1.wpn  = savplr[0].wpn;
  pl1.wpns = savplr[0].wpns;
  pl1.amul = savplr[0].amul;

  pl2.color = savplr[1].color;
  pl2.life = savplr[1].life;
  pl2.armor = savplr[1].armor;
  pl2.frag = savplr[1].frag;
  pl2.ammo = savplr[1].ammo;
  pl2.shel = savplr[1].shel;
  pl2.rock = savplr[1].rock;
  pl2.cell = savplr[1].cell;
  pl2.fuel = savplr[1].fuel;
  pl2.wpn  = savplr[1].wpn;
  pl2.wpns = savplr[1].wpns;
  pl2.amul = savplr[1].amul;

  res = 1;

_end1:
  MemCardClose();
_end2:
  MemCardStop();
  return res;
}

int mc_dosave(void) {
  int mc, new, i, slot, err;
  int res = 0;

  if (!yesnoscreen("DO YOU WANT TO SAVE?"))
    return res;

  MemCardStart();

  mc_scan();
  mc = mc_select();
  if (mc < 0)
    goto _end2;

  waitscreen("PLEASE WAIT", "CHECKING GAME DATA");

  memset(secbuf, 0, sizeof(secbuf));
  memset(&cfg, 0, sizeof(cfg));
  memset(&sav, 0, sizeof(sav));
  memset(&savhdr, 0, sizeof(savhdr));
  memset(savplr, 0, sizeof(savplr));

  new = 1;

  // check if there was a save already
  if (MemCardOpen(0x10 * mc, SAVNAME, O_RDONLY) == McErrNone) {
    if (MemCardReadData((long *)secbuf, 128 * 2, 128)) {
      while (!MemCardSync(1, &mccmd, mcresult + mc));
      if (mcresult[mc] == McErrNone) {
        memcpy(&cfg, secbuf, sizeof(cfg));
        new = memcmp(cfg.id, CFGMAGIC, 4);
        // corrupt file, zap it
        if (new) MemCardDeleteFile(0x10 * mc, SAVNAME);
      }
    }
    MemCardClose();
  }

  // fill in the config
  memcpy(cfg.id, CFGMAGIC, 4);
  cfg.snd_vol = snd_vol;
  cfg.mus_vol = mus_vol;
  for (i = 0; i < PLK_NUMKEYS; ++i) {
    cfg.p1keys[i] = *(int *)((u_char *)&pl1 + plk_ofs[i]);
    cfg.p2keys[i] = *(int *)((u_char *)&pl2 + plk_ofs[i]);
  }
  if (new) cfg.slotmask = 0;

  slot = selectslot(cfg.slotmask);
  if (slot <  0)
    goto _end2;

  if (cfg.slotmask & (1 << slot)) {
    if (!yesnoscreen("DO YOU WANT TO OVERWRITE THIS SAVE?"))
      goto _end2;
  }

  cfg.slotmask |= 1 << slot;

  waitscreen("PLEASE WAIT", "SAVING GAME");

  if (new) {
    printf("mc_dosave(): creating file\n");
    if ((err = MemCardCreateFile(0x10 * mc, SAVNAME, 1)) != McErrNone) {
      msgscreen("ERROR", "COULD NOT CREATE SAVE");
      goto _end2;
    }
  }

  if ((err = MemCardOpen(0x10 * mc, SAVNAME, O_WRONLY)) != McErrNone) {
    msgscreen("ERROR", "COULD NOT CREATE SAVE");
    goto _end2;
  }

  // fill in and write the header

  savhdr.id[0] = 'S'; savhdr.id[1] = 'C';
  savhdr.type = 0x11; // 1 frame
  savhdr.size = 1; // our saves fit in 1 block (8kb)
  memcpy(savhdr.clut, savicon_clut, sizeof(savicon_clut));
  str2sjis(SAVTITLE, savhdr.title);

  MemCardWriteData((u_long*)&savhdr, 128 * 0, 128);
  while (MemCardSync(1, &mccmd, &mcresult[mc]) == 0);
  if (mcresult[mc] != McErrNone) {
    msgscreen("ERROR", "COULD NOT WRITE HEADER");
    goto _end1;
  }

  // write the icon

  memcpy(secbuf, savicon, sizeof(savicon));

  MemCardWriteData((long *)secbuf, 128 * 1, 128);
  while (MemCardSync(1, &mccmd, &mcresult[mc]) == 0);
  if (mcresult[mc] != McErrNone) {
    msgscreen("ERROR", "COULD NOT WRITE ICON");
    goto _end1;
  }

  // write the config

  memcpy(secbuf, &cfg, sizeof(cfg));

  MemCardWriteData((long *)secbuf, 128 * 2, 128);
  while (MemCardSync(1, &mccmd, &mcresult[mc]) == 0);
  if (mcresult[mc] != McErrNone) {
    msgscreen("ERROR", "COULD NOT WRITE CONFIG");
    goto _end1;
  }

  // fill in and write the save
  memcpy(sav.id, SAVMAGIC, 4);
  strcpy(sav.map, "MAP00");
  sav.map[3] = '0' + g_map / 10;
  sav.map[4] = '0' + g_map % 10;
  sav.mode = g_dm;
  sav.numpl = _2pl + 1;

  savplr[0].color = pl1.color;
  savplr[0].life  = pl1.life;
  savplr[0].armor = pl1.armor;
  savplr[0].frag  = pl1.frag;
  savplr[0].ammo  = pl1.ammo;
  savplr[0].shel  = pl1.shel;
  savplr[0].rock  = pl1.rock;
  savplr[0].cell  = pl1.cell;
  savplr[0].fuel  = pl1.fuel;
  savplr[0].wpn   = pl1.wpn;
  savplr[0].wpns  = pl1.wpns;
  savplr[0].amul  = pl1.amul;

  savplr[1].color = pl2.color;
  savplr[1].life  = pl2.life;
  savplr[1].armor = pl2.armor;
  savplr[1].frag  = pl2.frag;
  savplr[1].ammo  = pl2.ammo;
  savplr[1].shel  = pl2.shel;
  savplr[1].rock  = pl2.rock;
  savplr[1].cell  = pl2.cell;
  savplr[1].fuel  = pl2.fuel;
  savplr[1].wpn   = pl2.wpn;
  savplr[1].wpns  = pl2.wpns;
  savplr[1].amul  = pl2.amul;

  memcpy(secbuf, &sav, sizeof(sav));
  memcpy(secbuf + sizeof(sav), savplr, sizeof(savplr));

  MemCardWriteData((long *)secbuf, 128 * (3 + slot), 128);
  while (MemCardSync(1, &mccmd, &mcresult[mc]) == 0);
  if (mcresult[mc] != McErrNone) {
    msgscreen("ERROR", "COULD NOT WRITE SAVE");
    goto _end1;
  }

  res = 1;

_end1:
  MemCardClose();
_end2:
  MemCardStop();
  return res;
}

void CFG_setdefaults(void) {
  // player 1 controls
  pl1.ku = K_P1_UP;
  pl1.kd = K_P1_DOWN;
  pl1.kl = K_P1_LEFT;
  pl1.kr = K_P1_RIGHT;
  pl1.kf = K_P1_SQUARE;
  pl1.kj = K_P1_CROSS;
  pl1.kp = K_P1_CIRCLE;
  pl1.kwl = K_P1_L1;
  pl1.kwr = K_P1_R1;
  pl1.ks = K_P1_R2;
  pl1.kh = K_P1_L2;
  // player 2 controls
  pl2.ku = K_P2_UP;
  pl2.kd = K_P2_DOWN;
  pl2.kl = K_P2_LEFT;
  pl2.kr = K_P2_RIGHT;
  pl2.kf = K_P2_SQUARE;
  pl2.kj = K_P2_CROSS;
  pl2.kp = K_P2_CIRCLE;
  pl2.kwl = K_P2_L1;
  pl2.kwr = K_P2_R1;
  pl2.ks = K_P2_R2;
  pl2.kh = K_P2_L2;
}
