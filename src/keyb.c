/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "keyb.h"
#include <stdlib.h>

#include <libetc.h>
#include <libpad.h>

unsigned int pad_btns = 0;
unsigned int pad_trig = 0;

static key_f * key_proc = NULL;

const char *K_name(int k) {
  static const char *knames[16] = {
    "L2",
    "R2",
    "L1",
    "R1",
    "TRIANGLE",
    "CIRCLE",
    "CROSS",
    "SQUARE",
    "SELECT",
    "L3",
    "R3",
    "START", 
    "UP",
    "RIGHT",
    "DOWN",
    "LEFT"
  };
  int i;
  for (i = 0; i < 32; ++i)
    if (k & (1 << i))
      return knames[i & 0xF];
}

void K_init(void) {
  PadInit(0);
}

void K_done(void) {
  PadStop();
}

// установить функцию обработки клавиш
void K_setkeyproc(key_f * k) {
  key_proc = k;
}

void K_update(void) {
  unsigned int pad = PadRead(0);
  pad_trig = pad & (pad ^ pad_btns);

  // run keyproc
  if (key_proc && pad_btns != pad) {
    unsigned int i, n, new;
    i = 1;
    for (n = 0; n < 32; ++n, i <<= 1) {
      new = pad & i;
      if (new != (pad_btns & i))
        key_proc(i, new);
    }
  }

  pad_btns = pad;
}
