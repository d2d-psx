/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "files.h"
#include "memory.h"
#include "vga.h"
#include "error.h"
#include "keyb.h"
#include "sound.h"
#include "view.h"
#include "player.h"
#include "switch.h"
#include "menu.h"
#include "misc.h"
#include "memcard.h"

#define QSND_NUM 14

enum {
  HIT100, ARMOR, JUMP, WPNS, IMMORTAL, SPEED, OPEN, EXIT
};

extern int PL_JUMP, PL_RUN;
extern byte _warp, cheat, p_fly;

extern byte g_music[8];

extern byte savname[7][24], savok[7];
void load_game(int);

static byte panim[] =
  "BBDDAACCDDAABBDDAACCDDAABBDDAACCDDAAEEEEEFEFEFEFEFEFEFEFEFEFEEEEE";
static byte * panimp = panim;

#define PCOLORN 10
int p1color = 0, p2color = 5;

static char _warp_mapname[9] = "MAP01";

enum {
  MENU, MSG
};
enum {
  CANCEL, NEWGAME, LOADGAME, OPTIONS, ENDGAME, ENDGM,
  PLR1, PLR2, COOP, DM, VOLUME, GAMMA, PLCOLOR, PLCEND, MUSIC, REMAP, PL1REMAP, PL2REMAP,
  SVOLM, SVOLP, MVOLM, MVOLP, GAMMAM, GAMMAP, PL1CM, PL1CP, PL2CM, PL2CP,
  MAPSELM, MAPSELP,
};

#ifndef DEMO
static int qsnd[QSND_NUM];
#endif

static char * main_txt[] = {
  "NEW GAME", "LOAD GAME", "OPTIONS"
}, * opt_txt[] = {
  "RESTART", "VOLUME", "BRIGHTNESS", "CONTROLS", "MUSIC"
}, * ngplr_txt[] = {
  "ONE PLAYER", "TWO PLAYERS"
}, * ngdm_txt[] = {
  "COOPERATIVE", "DEATHMATCH", "MAP"
}, * vol_txt[] = {
  "SOUND", "MUSIC"
}, * plcolor_txt[] = {
  "FIRST", "SECOND"
}, * gamma_txt[] = {
  ""
}, * controls_txt[] = {
  "PLAYER ONE", "PLAYER TWO",
};

static byte main_typ[] = {
  NEWGAME, LOADGAME, OPTIONS
}, ngplr_typ[] = {
  PLR1, PLR2
}, ngdm_typ[] = {
  COOP, DM, MAPSELM
}, opt_typ[] = {
  ENDGAME, VOLUME, GAMMA, REMAP, MUSIC
}, endgm_typ[] = {
  ENDGM, CANCEL
}, vol_typ[] = {
  SVOLM, MVOLM
}, plcolor_typ[] = {
  PL1CM, PL2CM
}, gamma_typ[] = {
  GAMMAM
}, controls_typ[] = {
  PL1REMAP, PL2REMAP
};

static menu_t main_mnu = {
  MENU, 3, 0, 80, "MENU", main_txt, main_typ
}, opt_mnu = {
  MENU, 5, 0, 75, "OPTIONS", opt_txt, opt_typ
}, ngplr_mnu = {
  MENU, 2, 0, 90, "NEW GAME", ngplr_txt, ngplr_typ
}, ngdm_mnu = {
  MENU, 3, 0, 90, "GAME TYPE", ngdm_txt, ngdm_typ
}, vol_mnu = {
  MENU, 2, 0, 40, "VOLUME", vol_txt, vol_typ
}, plcolor_mnu = {
  MENU, 2, 0, 90, "COLOR", plcolor_txt, plcolor_typ
}, gamma_mnu = {
  MENU, 1, 0, 85, "BRIGHTNESS", gamma_txt, gamma_typ
}, controls_mnu = {
  MENU, 2, 0, 90, "CONTROLS", controls_txt, controls_typ
}, endgm_msg = {
  MSG, 0, 0, 0, "RESTART LEVEL?", NULL, endgm_typ
};

static menu_t * mnu     = NULL;

static byte gm_redraw = 0;
static int gm_tm = 0;
unsigned int lastkey = 0;
static void * csnd1, * csnd2, * msnd1, * msnd2, * msnd3, * msnd4, * msnd5, * msnd6;
static int movsndt = 0;
static cacheimg * msklh[2], * mbarl, * mbarm, * mbarr, * mbaro, * mslotl, * mslotm, * mslotr;

static snd_t * voc = NULL;
static int voc_ch  = 0;

void G_keyf(int k, int press) {
  if (press)
    lastkey = k;
}

static void doremap(const char *title, player_t *pl) {
  int i, k, *kptr;
  int hlen = Z_linelens((char*)title);
  int klen;

  for (k = 0; k <= PLK_NUMKEYS; ++k) {
    pad_trig = 0;

    while (!pad_trig || (pad_trig & (K_P1_START | K_P2_START))) {
      V_startframe();

      G_draw();

      V_transclr(0, SCRW, 0, SCRH, 247);

      Z_gotoxy(96, 64);
      Z_printbf((char *)title);

      for (i = 0; i < k; ++i) {
        kptr = (int *)((u_char *)pl + plk_ofs[i]);
        Z_gotoxy(112, 80 + (i << 3));
        Z_printsf(plk_names[i]);
        Z_gotoxy(180, 80 + (i << 3));
        Z_printsf(K_name(*kptr));
      }

      Z_gotoxy(112, 80 + (k << 3));
      if (k < PLK_NUMKEYS) {
        Z_printsf(plk_names[k]);
        Z_gotoxy(180, 80 + (k << 3));
        Z_printsf("...");
      } else {
        Z_printsf("DONE. PRESS A KEY");
      }

      V_endframe();

      K_update();
    }

    if (k < PLK_NUMKEYS) {
      kptr = (int *)((u_char *)pl + plk_ofs[k]);
      *kptr = lastkey;
    }

    Z_sound(msnd2, 128);
  }

  VSync(3);
}

void GMV_stop(void) {
  if (voc) {
    if (voc_ch) {
      S_stop(voc_ch);
      voc_ch = 0;
    }
    free3(voc);
    voc = NULL;
  }
}

void GMV_say(char * nm) {
  /*
  int r, len;
  snd_t * p;
  byte * d;

  if ((r = F_findres(nm)) == -1)
    return;

  if (!(p = malloc3((len = F_getreslen(r)) + 16)))
    return;

  p->len    = len;
  p->rate   = 11000;
  p->lstart = p->llen = 0;
  GMV_stop();
  F_loadres(r, p + 1, 0, len);
  for (d = (byte *) (p + 1); len; --len, ++d)
    *d ^= 128;
  voc    = p;
  voc_ch = S_play(voc, -1, 1024, 255);
  */
}

void GM_set(menu_t * m) {
  mnu       = m;
  gm_redraw = 1;
  if (g_st == GS_GAME) {
    pl1.drawst=pl2.drawst=0xFF;
    V_setrect(0,SCRW,0,SCRH);
  }
}

void GM_command(int c) {
  int res;
  switch (c) {
      case CANCEL:
        GM_set(NULL);
        break;

      case MUSIC:
        F_freemus();
        F_nextmus(g_music);
        F_loadmus(g_music);
        S_startmusic(music_time * 2);
        GM_set(mnu);
        break;

      case NEWGAME:
        GMV_say("_NEWGAME");
        GM_set(&ngplr_mnu);
        break;

      case PLR2:
        GMV_say("_2PLAYER");
        GM_set(&ngdm_mnu);
        break;

      case PLR1:
        GMV_say("_1PLAYER");
        ngdm_mnu.cur = 0;

      case COOP:
      case DM:
        if (c == COOP)
          GMV_say("_COOP");
        else if (c == DM)
          GMV_say("_DM");
        if (c != PLR1) {
          GM_set(&plcolor_mnu);
          break;
        }

      case PLCEND:
        _2pl  = ngplr_mnu.cur;
        g_dm  = ngdm_mnu.cur;
        g_map = (_2pl && _warp > 1) ? _warp : 1;
        g_loaded = 0;
        PL_reset();
        if (_2pl) {
          pl1.color = p1color;
          pl2.color = p2color;
        } else {
          pl1.color = 0;
        }
        G_start();
        GM_set(NULL);
        break;

      case OPTIONS:
        GMV_say("_RAZNOE");
        GM_set(&opt_mnu);
        break;

      case LOADGAME:
        S_stopmusic();
        res = mc_doload();
        K_setkeyproc(G_keyf);
        S_startmusic(0);
        if (res) {
          g_loaded = 1;
          PL_reset();
          G_start();
          g_loaded = 0;
          GM_set(NULL);
        }
        break;

      case VOLUME:
        GMV_say("_VOLUME");
        GM_set(&vol_mnu);
        break;

      case GAMMA:
        GMV_say("_GAMMA");
        GM_set(&gamma_mnu);
        break;

      case REMAP:
        GM_set(&controls_mnu);
        break;

      case ENDGAME:
        if (g_st != GS_GAME)
          break;
        GMV_say("_RESTART");
        GM_set(&endgm_msg);
        break;

      case ENDGM:
        PL_reset();
        G_start();
        GM_set(NULL);
        break;

      case PL1CM:
        if (--p1color < 0)
          p1color = PCOLORN - 1;
        break;

      case PL1CP:
        if (++p1color >= PCOLORN)
          p1color = 0;
        break;

      case PL2CM:
        if (--p2color < 0)
          p2color = PCOLORN - 1;
        break;

      case PL2CP:
        if (++p2color >= PCOLORN)
          p2color = 0;
        break;

      case PL1REMAP:
        doremap("PLAYER ONE", &pl1);
        break;

      case PL2REMAP:
        doremap("PLAYER TWO", &pl2);
        break;

      case MAPSELM:
        if (_warp > 1)
          --_warp;
        else
          _warp = NUMMAPS;
        F_getmapname(_warp_mapname, _warp);
        break;

      case MAPSELP:
        if (_warp < NUMMAPS)
          _warp++;
        else
          _warp = 1;
        F_getmapname(_warp_mapname, _warp);
        break;

      case SVOLM:
        S_volume(snd_vol - 8);
        break;

      case SVOLP:
        S_volume(snd_vol + 8);
        break;

      case MVOLM:
        S_volumemusic(mus_vol - 8);
        break;

      case MVOLP:
        S_volumemusic(mus_vol + 8);
        break;

      case GAMMAM:
        // setgamma(gammaa - 1);
        break;

      case GAMMAP:
        // setgamma(gammaa + 1);
        break;
  }
} /* GM_command */

int GM_act(void) {
  byte c;

  if (mnu == &plcolor_mnu) {
    if (*(++panimp) == 0)
      panimp = panim;
    GM_set(mnu);
  }
  if (movsndt > 0)
    --movsndt;
  else
    movsndt = 0;
  if (g_st == GS_TITLE) {
    if (!mnu) {
      if (lastkey) {
        GM_set(&main_mnu);
        Z_sound(msnd3, 128);
        lastkey = 0;
        return 1;
      }
    }
  }
  switch (lastkey) {
      case K_P1_START: // case 1:
        if (!mnu) {
          GM_set(&main_mnu);
          Z_sound(msnd3, 128);
        } else {
          GM_set(NULL);
          Z_sound(msnd4, 128);
        }
        break;
      /*
      case SDLK_F5:
        if (mnu)
          break;
        Z_sound(msnd3, 128);
        GMV_say("_GAMMA");
        GM_set(&gamma_mnu);
        break;

      case SDLK_F4: // case 0x3E:
        if (mnu)
          break;
        Z_sound(msnd3, 128);
        GMV_say("_VOLUME");
        GM_set(&vol_mnu);
        break;

      case SDLK_F2: // case 0x3C:
        if (mnu)
          break;
        if (g_st != GS_GAME)
          break;
        Z_sound(msnd3, 128);
        F_getsavnames();
        GM_set(&save_mnu);
        break;

      case SDLK_F3: // case 0x3D:
        if (mnu)
          break;
        Z_sound(msnd3, 128);
        F_getsavnames();
        GM_set(&load_mnu);
        break;

      case SDLK_F10: // case 0x44:
        if (mnu)
          break;
        Z_sound(msnd3, 128);
        GM_command(QUITGAME);
        break;
      */
      case K_P1_UP:
        if (!mnu)
          break;
        if (mnu->type != MENU)
          break;
        if (--mnu->cur < 0)
          mnu->cur = mnu->n - 1;
        GM_set(mnu);
        Z_sound(msnd1, 128);
        break;

      case K_P1_DOWN:
        if (!mnu)
          break;
        if (mnu->type != MENU)
          break;
        if (++mnu->cur >= mnu->n)
          mnu->cur = 0;
        GM_set(mnu);
        Z_sound(msnd1, 128);
        break;

      case K_P1_LEFT:
      case K_P1_RIGHT:
        if (!mnu)
          break;
        if (mnu->type != MENU)
          break;
        if (mnu->t[mnu->cur] < SVOLM)
          break;
        GM_command(mnu->t[mnu->cur] + ((lastkey == K_P1_LEFT) ? 0 : 1)); // GM_command(mnu->t[mnu->cur]+((lastkey==0x4B || lastkey==0xCB)?0:1));
        GM_set(mnu);
        if (!movsndt)
          movsndt = Z_sound((lastkey == K_P1_LEFT) ? msnd5 : msnd6, 255);  // if(!movsndt) movsndt=Z_sound((lastkey==0x4B || lastkey==0xCB)?msnd5:msnd6,255);
        break;

      case K_P1_CROSS:
        if (!mnu)
          break;
        if (mnu->type == MSG) {
          Z_sound(msnd3, 128);
          GM_command(mnu->t[0]);
        } else if (mnu->type == MENU) {
          if (mnu->t[mnu->cur] >= PL1CM && mnu->t[mnu->cur] < MAPSELM) {
            Z_sound(msnd2, 128);
            GM_command(PLCEND);
            break;
          }
          if (mnu->t[mnu->cur] >= SVOLM)
            break;
          Z_sound(msnd2, 128);
          GM_command(mnu->t[mnu->cur]);
        }
        break;

      case K_P1_CIRCLE: // case 0x31:
        if (!mnu)
          break;
        if (mnu->type != MSG)
          GM_set(NULL); // exit out as if we pressed START
        else
          GM_command(mnu->t[1]);
        Z_sound(msnd4, 128);
        break;

      /*
      case SDLK_F1: // case 0x3B:
        if (shot_vga) {
          shot();
          Z_sound(msnd4, 128);
        }
        break;
      */
  }
  lastkey = 0;
  return (mnu) ? 1 : 0;
} /* GM_act */

void GM_init(void) {
  #ifndef DEMO
  int i;
  static char nm[QSND_NUM][6] = {
    "CYBSIT", "KNTDTH", "MNPAIN", "PEPAIN", "SLOP",   "MANSIT", "BOSPN", "VILACT",
    "PLFALL", "BGACT",  "BGDTH2", "POPAIN", "SGTATK", "VILDTH"
  };
  char s[8];

  s[0] = 'D';
  s[1] = 'S';
  for (i = 0; i < QSND_NUM; ++i) {
    memcpy(s + 2, nm[i], 6);
    qsnd[i] = F_getresid(s);
  }
  #endif /* ifndef DEMO */
  csnd1    = Z_getsnd("HAHA1");
  csnd2    = Z_getsnd("RADIO");
  msnd1    = Z_getsnd("PSTOP");
  msnd2    = Z_getsnd("PISTOL");
  msnd3    = Z_getsnd("SWTCHN");
  msnd4    = Z_getsnd("SWTCHX");
  msnd5    = Z_getsnd("SUDI");
  msnd6    = Z_getsnd("TUDI");
  msklh[0] = V_cachepic(M_lock(F_getresid("M_SKULL1")));
  //  msklh[0]=load_vga("vga\\spr.vga","M_SKULL1");
  msklh[1] = V_cachepic(M_lock(F_getresid("M_SKULL2")));
  mbarl    = V_cachepic(M_lock(F_getresid("M_THERML")));
  mbarm    = V_cachepic(M_lock(F_getresid("M_THERMM")));
  mbarr    = V_cachepic(M_lock(F_getresid("M_THERMR")));
  mbaro    = V_cachepic(M_lock(F_getresid("M_THERMO")));
  mslotl   = V_cachepic(M_lock(F_getresid("M_LSLEFT")));
  mslotm   = V_cachepic(M_lock(F_getresid("M_LSCNTR")));
  mslotr   = V_cachepic(M_lock(F_getresid("M_LSRGHT")));
  K_setkeyproc(G_keyf);
} /* GM_init */

int GM_draw(void) {
  int i, j, k, y;
  char mapname[8];

  ++gm_tm;
  V_setrect(0, SCRW, 0, SCRH); // V_setrect(0,320,0,200);
  if (!mnu && !gm_redraw)
    return 0;

  gm_redraw = 0;
  if (!mnu)
    return 1;

  if (mnu->type == MENU) {
    y = (200 - mnu->n * 16 - 20) / 2;
    Z_gotoxy(mnu->x, y - 10);
    Z_printbf(mnu->ttl);
    for (i = 0; i < mnu->n; ++i) {
      Z_gotoxy(mnu->x + ((mnu->t[i] >= SVOLM) ? ((mnu->t[i] >= PL1CM) ? (mnu->t[i] >= MAPSELM ? 0 : 50) : 152) : 0), y + i * 16 + 20);
      Z_printbf(mnu->m[i]);
      if (mnu->t[i] == MUSIC) {
        Z_printbf(" '%.8s'", g_music);
      } else if (mnu->t[i] >= MAPSELM) {
        Z_printbf(" '%.8s'", _warp_mapname);
      } else if (mnu->t[i] >= PL1CM) {
        V_manspr(mnu->x + ((mnu->t[i] == PL1CM) ? 15 : 35), y + i * 16 + 20 + 14,
          PL_getspr(*panimp, 0),
          ((mnu->t[i] == PL1CM) ? p1color : p2color)
        );
      } else if (mnu->t[i] >= SVOLM) {
        V_spr(mnu->x, j = y + i * 16 + 20, mbarl);
        for (k = 8; k < 144; k += 8)
          V_spr(mnu->x + k, j, mbarm);
        V_spr(mnu->x + 144, j, mbarr);
        switch (mnu->t[i]) {
            case SVOLM:
              k = snd_vol;
              break;

            case MVOLM:
              k = mus_vol;
              break;

            case GAMMAM:
              k = 0;
              // k = gammaa << 5;
              break;
        }
        V_spr(mnu->x + 8 + k, j, mbaro);
      }
    }
    V_spr(mnu->x - 25, y + mnu->cur * 16 + 20 - 8, msklh[(gm_tm / 6) & 1]);
  } else {
    Z_gotoxy((320 - strlen(mnu->ttl) * 7) / 2, 90);
    Z_printsf(mnu->ttl);
    Z_gotoxy(98, 100);
    Z_printsf("(X - YES / O - NO)");
  }
  return 1;
} /* GM_draw */
