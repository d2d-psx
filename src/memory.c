/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <libapi.h>
#include "error.h"
#include "files.h"
#include "memory.h"

dword dpmi_memavl(void);

extern int d_start, d_end;

extern mwad_t wad[];

static byte m_active = FALSE;

static void * resp[MAX_WAD];
static short resl[MAX_WAD];

void M_startup(void *heapbase, unsigned long heapsize) {
  if (m_active)
    return;

  logo("M_startup(): memory init: %u kb heap at %p\n", heapsize >> 10, heapbase);

  InitHeap3((void*)heapbase, heapsize);

  memset(resp, 0, sizeof(resp));
  memset(resl, 0, sizeof(resl));
  //  logo("  свободно DPMI-памяти: %uK\n",dpmi_memavl()>>10);
  m_active = TRUE;
}

void M_shutdown(void) {
  if (!m_active)
    return;

  m_active = FALSE;
}

static void allocres(int h) {
  int *p;
  if (!(p = malloc3(wad[h].l + 4)))
    ERR_fatal("M_lock: malloc3 failed");
  *p = h;
  ++p;
  resp[h] = p;
  F_loadres(h, p, 0, wad[h].l);
}

void * M_lock(int h) {
  if ((h == -1) || (h == 0xFFFF))
    return NULL;

  h &= -1 - 0x8000;
  if (h >= MAX_WAD)
    ERR_fatal("M_lock: funny resource number");
  if (!resl[h]) {
    if (!resp[h])
      allocres(h);
  }
  ++resl[h];
  return resp[h];
}

void M_unlock(void * p) {
  int h;

  if (!p)
    return;

  h = ((int *) p)[-1];
  if (h >= MAX_WAD)
    ERR_fatal("M_unlock: funny resource number");
  if (!resl[h])
    return;

  --resl[h];

  if (!resl[h]) {
    // printf("M_unlock(): freeing resource %x\n", h);
    free3((int*)p - 1);
    resp[h] = NULL;
  }
}
