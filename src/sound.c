/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include "files.h"
#include "sound.h"
#include "error.h"

#include <sys/types.h>
#include <libspu.h>

#define MAXSPUSND 160
#define MAXSND 256
#define SNDBASENOTE 88
#define VAGOFS 48

static u_char spu_alloctable[(MAXSPUSND + 1) * SPU_MALLOC_RECSIZ];

static cachesnd sndcache[MAXSND];

SpuVoiceAttr chanattr;
int lastchan = 0;

short snd_vol = 50;

int snddisabled = 1;

void S_init(void) {
  int i;
  SpuInit();
  SpuInitMalloc(MAXSPUSND, spu_alloctable);
  for (i = 0; i < MAXSND; ++i)
    sndcache[i].spuaddr = -1;
  snddisabled = 0;
  S_volume(snd_vol);
  SpuSetCommonMasterVolume(0x3fff, 0x3fff);
}

void S_done(void) {
  SpuQuit();
}

short S_play(cachesnd * s, short c, unsigned r, short v) {
  if (snddisabled || !s)
    return 0;

  S_loadsound(s);

  if (c < 0) {
    c = lastchan;
    lastchan = (lastchan + 1) & (SFX_CHANS - 1);
  }

  chanattr.voice = 1L << (SFX_CH0 + c);
  chanattr.mask = SPU_VOICE_VOLL | SPU_VOICE_VOLR | SPU_VOICE_WDSA | SPU_VOICE_SAMPLE_NOTE | SPU_VOICE_NOTE;
  chanattr.volume.left = chanattr.volume.right = (snd_vol * v) >> 1;
  chanattr.addr = s->spuaddr;
  chanattr.sample_note = SNDBASENOTE << 8;
  chanattr.note = 64 << 8;

  SpuSetKeyOnWithAttr(&chanattr);
}

void S_stop(short c) {
  if (c < 0)
    SpuSetKey(SPU_OFF, SFX_MASK);
  else
    SpuSetKey(SPU_OFF, 1L << c);
}

void S_volume(int v) {
  if (snddisabled)
    return;

  snd_vol = v;
  if (snd_vol > 127)
    snd_vol = 127;
  if (snd_vol < 0)
    snd_vol = 0;
}

void S_wait(void) {
  if (snddisabled)
    return;
}

void S_clearmem(void) {
  int i;
  for (i = 0; i < MAXSND; ++i)
    if (sndcache[i].spuaddr != -1)
      S_unloadsound(sndcache + i);
}

cachesnd *S_cachesound(snd_t *snd) {
  int i;
  for (i = 0; i < MAXSND; ++i) {
    if (!sndcache[i].snd && sndcache[i].spuaddr == -1) {
      sndcache[i].snd = (u_char *)snd;
      sndcache[i].spuaddr = -1;
      sndcache[i].rate = bswap32(snd->rate);
      sndcache[i].size = F_getreslen(((int *)snd)[-1]) - VAGOFS;
      return &sndcache[i];
    } else if (sndcache[i].snd == (u_char *)snd) {
      // already cached
      return &sndcache[i];
    }
  }
  ERR_fatal("S_cachesound(): ran out of cache space!\n");
  return NULL;
}

void S_uncachesound(cachesnd *csnd) {
  if (!csnd || !csnd->snd)
    return;
  S_unloadsound(csnd);
  M_unlock(csnd->snd);
  csnd->snd = NULL;
}

void S_unloadsound(cachesnd *csnd) {
  if (!csnd || csnd->spuaddr == -1)
    return;
  SpuFree(csnd->spuaddr);
  csnd->spuaddr = -1;
}

void S_loadsound(cachesnd *csnd) {
  int i, n;

  if (csnd->spuaddr != -1 || !csnd->snd)
    return; // already loaded

  csnd->spuaddr = SpuMalloc(csnd->size);
  if (csnd->spuaddr == -1) {
    printf("S_loadsound(): SpuMalloc failed, unloading some sounds\n");
    for (i = MAXSND-1; csnd->spuaddr == -1 && i > 16; --i) {
      if (sndcache[i].spuaddr != -1) {
        S_unloadsound(sndcache + i);
        csnd->spuaddr = SpuMalloc(csnd->size);
      }
    }
    if (csnd->spuaddr == -1)
      ERR_fatal("S_loadsound(): cannot make space for sound!\n");
  }

  SpuIsTransferCompleted(SPU_TRANSFER_WAIT); // wait for previous transfers to complete
  SpuSetTransferMode(SpuTransByDMA);
  SpuSetTransferStartAddr(csnd->spuaddr);
  SpuWrite(csnd->snd + VAGOFS, csnd->size);
}

int Z_sound(cachesnd *s, int v) {
  // if(snd_type==-1) return 0;
  if (!s) return 0;
  S_play(s, -1, 1024, v);
  return s->size >> 10; // return F_getreslen(((int*)s)[-1])/605;
}
