/*
 * Управление графикой VGA для DOS4GW
 * Модуль версии 1.0
 * Copyright (C) Алексей Волынсков, 1996
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifndef _VGA_H
#define _VGA_H

#ifdef __cplusplus
extern "C" {
#endif

#define VRAM_PAL_X 0
#define VRAM_PAL_Y (SCRH << 1)
#define VRAM_PAL_W 256
#define VRAM_PAL_H 1

// заголовок изображения
#pragma pack(1)
typedef struct {
  unsigned short w, h;   // W-ширина,H-высота
  short          sx, sy; // сдвиг центра изображения
} vgaimg;

// R-красный,G-зеленый,B-синий
typedef struct {
  unsigned char r, g, b;
} rgb_t;
#pragma pack()

typedef struct cacheimg_s {
  vgaimg *img;
  unsigned short tpage;
  unsigned char u, v;
} cacheimg;

// 256-и цветовая палитра VGA
typedef rgb_t vgapal[256];

// карта цветов
typedef unsigned char colormap[256];

// тип функции перерисовки экрана
typedef void redraw_f (void);

// переключение в режим VGA 320x200,256 цветов
// возвращает 0, если все о'кей
short V_init(void);

// переключение в текстовый режим
void V_done(void);

// ждать обратного хода луча развертки
void V_wait(void);

// вывести картинку i в координатах (x,y)
void V_pic(short x, short y, cacheimg * i);
void V_bigpic(int x, int y, cacheimg *i);

void smoke_sprf(int x, int y, unsigned char c);
void flame_sprf(int x, int y, unsigned char c);

// вывести спрайт i в координатах (x,y)
void V_spr(short x, short y, cacheimg * i);

// вывести зеркально перевернутый спрайт i в координатах (x,y)
void V_spr2(short x, short y, cacheimg * i);

// вывести точку цвета c в координатах (x,y)
void V_dot(short x, short y, unsigned char c);

void V_manspr(int x, int y, cacheimg * p, unsigned char c);

void V_manspr2(int x, int y, cacheimg * p, unsigned char c);


// очистить прямоугольник цветом c
// x-левая сторона,w-ширина,y-верх,h-высота
void V_clr(short x, short w, short y, short h, unsigned char c);
void V_transclr(short x, short w, short y, short h, unsigned char c);
void V_blendclr(short x, short w, short y, short h, short bl, unsigned char r, unsigned char g, unsigned char b);

// получить текущую палитру в массив p
void VP_getall(void * p);

// установить палитру из массива p
void VP_setall(void * p);

// установить n цветов, начиная с f, из массива p
void VP_set(void * p, short f, short n);

// заполнить палитру одним цветом (r,g,b)
void VP_fill(char r, char g, char b);

// установить область вывода
void V_setrect(short x, short w, short y, short h);

// установить адрес экранного буфера
// NULL - реальный экран
void V_setscr(void *);

// скопировать прямоугольник на экран
void V_copytoscr(short x, short w, short y, short h);

void V_maptoscr(int, int, int, int, void *);

#define V_uploadpic(cimg) if ((cimg)->tpage == 0xFFFF) V_fitpic(cimg)
void V_fitpic(cacheimg *cimg);
cacheimg *V_cachepic(vgaimg *img);
void V_uncachepic(cacheimg *cimg);
void V_clearmem(void);

// область вывода
extern short scrw, scrh, scrx, scry;
extern int override_pal;

void V_rotspr(int x, int y, cacheimg * i, int d);
void V_center(int f);
void V_offset(int ox, int oy);
void V_setclip(short x, short w, short y, short h);

void V_startframe(void);
void V_endframe(void);

void Z_loadingscreen(unsigned int time);
void Z_clearscreen(int r, int g, int b);

extern char fullscreen;

extern vgapal main_pal;

#ifdef __cplusplus
}
#endif

#endif // _VGA_H
