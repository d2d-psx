/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include "memory.h"
#include "files.h"
#include "sound.h"
#include "error.h"

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <sys/types.h>
#include <libetc.h>
#include <libcd.h>
#include <libspu.h>

#define DMM_MAGIC "DMM\0"
#define DMM_MAXVER 0
#define DMM_NCH 8
#define BASENOTE 93

#pragma pack(push, 1)
typedef struct dmm_note {
  u_char note;
  u_char instr;
  u_char vol;
  u_char ticks;
} dmm_note_t;

typedef struct dmm_header {
  char id[4]; // DMM\0
  u_char ver;
  u_char numpats;
  u_short numnotes;
} dmm_header_t;

typedef struct dmm_instrid {
  u_char type;
  char name[13];
  u_short res;
} dmm_instrid_t;
#pragma pack(pop)

typedef struct { dmm_note_t *ch[DMM_NCH]; } dmm_pos_t;

typedef struct {
  dmm_note_t *pos;
  int delay;
  int instr;
} dmm_chan_t;

short mus_vol = 50;
int music_time = 3;
int musdisabled = 1;

static volatile int musplaying = 0;

static dmm_header_t *dmm_hdr;
static dmm_note_t *dmm_notes;
static dmm_pos_t *dmm_patstart;

static int dmm_seqlen = 0;
static u_char *dmm_seq;

static int dmm_numinstr = 0;
static dmm_instrid_t *dmm_instrid;
static cachesnd **dmm_instr;

static int dmm_nextseq = 0;
static dmm_chan_t dmm_chans[DMM_NCH];

static long musevent = -1;

extern SpuVoiceAttr chanattr;
static SpuLVoiceAttr dmm_attr[DMM_NCH];
static unsigned long dmm_onvoice = 0;

static inline void dmm_chans_clear(void) {
  int i;

  // mute and kill off all the channels
  chanattr.voice = MUS_MASK;
  chanattr.mask = SPU_VOICE_VOLL | SPU_VOICE_VOLR | SPU_VOICE_NOTE;
  chanattr.volume.left = chanattr.volume.right = 0;
  chanattr.note = 0;
  SpuSetVoiceAttr(&chanattr);
  SpuSetKey(SPU_OFF, MUS_MASK);

  for (i = 0; i < DMM_NCH; ++i) {
    dmm_chans[i].pos = NULL;
    dmm_chans[i].delay = 0;
    dmm_chans[i].instr = -1;
  }
}

static inline int dmm_setseq(void) {
  int pnum, i;
  dmm_chans_clear();
  for (;;) {
    if (dmm_nextseq >= dmm_seqlen) return 0;
    if ((pnum = dmm_seq[dmm_nextseq++]) != 0xFF) break;
    if (dmm_nextseq >= dmm_seqlen) return 0;
    dmm_nextseq = dmm_seq[dmm_nextseq];
  }
  if (pnum >= dmm_hdr->numpats) {
    pnum = 0xFFFF;
    return 0;
  }
  for (i = 0; i < DMM_NCH; ++i)
    dmm_chans[i].pos = dmm_patstart[pnum].ch[i];
  return 1;
}

static int dmm_chan_tick(int chn) {
  dmm_chan_t *ch = dmm_chans + chn;
  dmm_note_t *cur;
  SpuLVoiceAttr *chattr = dmm_attr + chn;

  chattr->voiceNum = MUS_CH0 + chn;
  // HACK: always set a dummy attr because mask 0 is all attributes
  // (what the fuck, Sony?)
  chattr->attr.mask = SPU_VOICE_VOLMODEL;
  chattr->attr.volmode.left = SPU_VOICE_DIRECT;

  for (;;) {
    if (!ch->pos) return 0; // channel dummied out
    if (--(ch->delay) >= 0) return 1; // playing something
    cur = ch->pos++;
    if (cur > dmm_notes + dmm_hdr->numnotes || cur->vol == 0x80) {
      // channel pattern ended, dummy it out
      ch->pos = NULL;
      ch->delay = 0x7FFFFFFF;
      return 0;
    }
    if (cur->note == 0xFE) {
      // long pause
      ch->delay = cur->ticks + 0x100 * cur->instr;
      if (!ch->delay) ch->delay = 0x10000;
      chattr->attr.mask = SPU_VOICE_VOLL | SPU_VOICE_VOLR;
      chattr->attr.volume.left = chattr->attr.volume.right = 0;
    } else {
      // always set volume
      chattr->attr.mask = SPU_VOICE_VOLL | SPU_VOICE_VOLR;
      chattr->attr.volume.left = chattr->attr.volume.right = (cur->vol * mus_vol) >> 1;
      // set instrument if it changed
      if (cur->instr && cur->instr != ch->instr) {
        chattr->attr.mask |= SPU_VOICE_WDSA | SPU_VOICE_SAMPLE_NOTE;
        chattr->attr.sample_note = BASENOTE << 8;
        chattr->attr.addr = dmm_instr[cur->instr-1]->spuaddr;
        ch->instr = cur->instr;
      }
      // set note if it's an actual note
      if (cur->note < 0xFE) {
        chattr->attr.mask |= SPU_VOICE_NOTE;
        chattr->attr.note = (64 - 24 + cur->note) << 8;
      }
      // we'll key-on this channel on tick end
      dmm_onvoice |= 1 << (MUS_CH0 + chn);
      // delay 0 is actually 256
      ch->delay = cur->ticks ? cur->ticks : 0x100;
    }
  }
}

static int dmm_read(char resname[8]) {
  u_char *data;
  int i, j, chpos;

  printf("dmm_read(): reading song %.8s\n", resname);

  data = M_lock(F_getresid(resname));
  if (!data) ERR_fatal("dmm_read(): out of memory\n");

  dmm_hdr = (dmm_header_t *)data;
  if (memcmp(dmm_hdr->id, DMM_MAGIC, 4) || dmm_hdr->ver > DMM_MAXVER) {
    printf("dmm_read(): not a valid DMM file\n");
    M_unlock(data); dmm_hdr = NULL;
    return 0;
  }

  printf("dmm_read(): DMMv%d, %d pats, %d notes\n", (int)dmm_hdr->ver, (int)dmm_hdr->numpats, (int)dmm_hdr->numnotes);

  data += sizeof(dmm_header_t);
  dmm_notes = (dmm_note_t *)data;

  data += sizeof(dmm_note_t) * dmm_hdr->numnotes;
  dmm_seqlen = *(u_char *)(data++);
  dmm_seq = data;

  data += dmm_seqlen;
  dmm_numinstr = *(u_char *)(data++);
  dmm_instrid = (dmm_instrid_t *)data;
  dmm_instr = calloc3(dmm_numinstr, sizeof(cachesnd*));
  if (!dmm_instr) ERR_fatal("dmm_read(): out of memory (2)\n");

  // assume correct instrument names
  printf("dmm_read(): caching %d instruments\n", dmm_numinstr);
  for (i = 0; i < dmm_numinstr; ++i)
    if (dmm_instrid[i].name[0])
      dmm_instr[i] = S_cachesound(M_lock(F_getresid(dmm_instrid[i].name)));

  // calculate pattern start offsets
  dmm_patstart = calloc3(dmm_hdr->numpats, sizeof(dmm_pos_t));
  if (!dmm_patstart) ERR_fatal("dmm_read(): out of memory (3)\n");
  chpos = 0;
  for (i = 0; i < dmm_hdr->numpats; ++i) {
    for (j = 0; j < DMM_NCH; ++j) {
      dmm_patstart[i].ch[j] = dmm_notes + chpos;
      while (chpos < dmm_hdr->numnotes && dmm_notes[chpos].vol != 0x80)
        ++chpos;
      if (chpos < dmm_hdr->numnotes) ++chpos;
    }
  }

  dmm_nextseq = 0;
  dmm_setseq();

  return 1;
}

void S_initmusic(void) {
  musdisabled = 0;
  musplaying = 0;
  S_volumemusic(mus_vol);
}

void S_donemusic(void) {
  F_freemus();
}

void S_startmusic(int time) {
  musplaying = 1;
  SpuSetKey(SPU_ON, dmm_onvoice);
}

void S_stopmusic(void) {
  musplaying = 0;
  SpuSetKey(SPU_OFF, MUS_MASK);
}

void S_volumemusic(int v) {
  if (musdisabled)
    return;

  mus_vol = v;
  if (mus_vol > 127)
    mus_vol = 127;
  if (mus_vol < 0)
    mus_vol = 0;
  /*
  chanattr.voice = MUS_MASK;
  chanattr.mask = SPU_VOICE_VOLL | SPU_VOICE_VOLR;
  chanattr.volume.left = chanattr.volume.right = mus_vol << 7;
  SpuSetVoiceAttr(&chanattr);
  */
}

void F_loadmus(char n[8]) {
  int i;

  if (musdisabled)
    return;

  if (!dmm_read(n)) {
    printf("F_loadmus(%.8s): dmm_read() FAILED\n", n);
    return;
  }

  for (i = 0; i < dmm_numinstr; ++i)
    S_loadsound(dmm_instr[i]);
}

void F_freemus(void) {
  int i;

  if (!dmm_hdr)
    return;

  S_stopmusic();

  for (i = 0; i < dmm_numinstr; ++i)
    S_uncachesound(dmm_instr[i]);

  free3(dmm_instr); dmm_instr = NULL; dmm_numinstr = 0;
  free3(dmm_patstart); dmm_patstart = NULL;
  M_unlock(dmm_hdr); dmm_hdr = NULL;
}

void S_updatemusic(void) {
  int i, live = 0;

  if (!musplaying) return;

  // we'll only key-on the voices that have a new note on them
  dmm_onvoice = 0;

  for (i = 0; i < DMM_NCH; ++i)
    if (dmm_chan_tick(i))
      live = 1; // at least something is playing on this pattern

  if (!live) {
    if (!dmm_setseq()) {
      // song ended, no loops?
      S_stopmusic();
      return;
    }
  }

  // send attrs and press the keys that need it
  SpuLSetVoiceAttr(DMM_NCH, dmm_attr);
  SpuSetKey(SPU_ON, dmm_onvoice);
}
