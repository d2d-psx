/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) fgsfds 2019
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#include "glob.h"
#include <stdio.h>
#include <string.h>
// #include <conio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <malloc.h>
#include "keyb.h"
#include "sound.h"
#include "vga.h"
#include "memory.h"
#include "files.h"
#include "error.h"
#include "memcard.h"

#include <ctype.h>
#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <libspu.h>

// length of morse stuff in vblanks
#define DIT 2
#define DAH 6
#define LSP DAH
#define WSP (7 * DIT)

#define SINADDR 0x1100
#define SINCH SPU_16CH

// ERR_ messages are now defined in error.h

extern SpuVoiceAttr chanattr;

static const u_char morse[][5] = {
  // digits first
  { DAH, DAH, DAH, DAH, DAH, }, // 0
  { DIT, DAH, DAH, DAH, DAH, }, // 1
  { DIT, DIT, DAH, DAH, DAH, }, // 2
  { DIT, DIT, DIT, DAH, DAH, }, // 3
  { DIT, DIT, DIT, DIT, DAH, }, // 4
  { DIT, DIT, DIT, DIT, DIT, }, // 5
  { DAH, DIT, DIT, DIT, DIT, }, // 6
  { DAH, DAH, DIT, DIT, DIT, }, // 7
  { DAH, DAH, DAH, DIT, DIT, }, // 8
  { DAH, DAH, DAH, DAH, DIT, }, // 9
  // letters
  { DIT, DAH,                }, // A
  { DAH, DIT, DIT, DIT,      }, // B
  { DAH, DIT, DAH, DIT,      }, // C
  { DAH, DIT, DIT,           }, // D
  { DIT,                     }, // E
  { DIT, DIT, DAH, DIT,      }, // F
  { DAH, DAH, DIT,           }, // G
  { DIT, DIT, DIT, DIT,      }, // H
  { DIT, DIT,                }, // I
  { DIT, DAH, DAH, DAH,      }, // J
  { DAH, DIT, DAH,           }, // K
  { DIT, DAH, DIT, DIT,      }, // L
  { DAH, DAH,                }, // M
  { DAH, DIT,                }, // N
  { DAH, DAH, DAH,           }, // O
  { DIT, DAH, DAH, DIT,      }, // P
  { DAH, DAH, DIT, DAH,      }, // Q
  { DIT, DAH, DIT,           }, // R
  { DIT, DIT, DIT,           }, // S
  { DAH,                     }, // T
  { DIT, DIT, DAH,           }, // U
  { DIT, DIT, DIT, DAH,      }, // V
  { DIT, DAH, DAH,           }, // W
  { DAH, DIT, DIT, DAH,      }, // X
  { DAH, DIT, DAH, DAH,      }, // Y
  { DAH, DAH, DIT, DIT,      }, // Z
};

static const u_char sinewave[] = {
  0x26, 0x06, 0x30, 0x10, 0x01, 0x11, 0x01, 0x21,
  0x10, 0x21, 0x20, 0x11, 0x21, 0x11, 0x12, 0x21, 
  0x27, 0x02, 0x24, 0x33, 0x34, 0x43, 0x33, 0x44,
  0x33, 0x35, 0x53, 0x43, 0x34, 0x35, 0x44, 0x44, 
  0x27, 0x02, 0x34, 0x35, 0x35, 0x44, 0x44, 0x34,
  0x35, 0x44, 0x34, 0x44, 0x43, 0x43, 0x34, 0x43, 
  0x27, 0x02, 0x33, 0x24, 0x34, 0x33, 0x42, 0x32,
  0x23, 0x23, 0x32, 0x22, 0x13, 0x22, 0x12, 0x22, 
  0x28, 0x02, 0x32, 0x32, 0x22, 0x22, 0x21, 0x20,
  0x01, 0x01, 0x00, 0x00, 0x0f, 0x0e, 0xef, 0xfe, 
  0x27, 0x02, 0xff, 0xef, 0xff, 0xef, 0xfe, 0xfd,
  0xee, 0xee, 0xfd, 0xdd, 0xde, 0xde, 0xdd, 0xce, 
  0x27, 0x02, 0xce, 0xdd, 0xdd, 0xdc, 0xcd, 0xdc,
  0xcd, 0xdc, 0xcc, 0xdc, 0xcc, 0xcc, 0xbd, 0xcd, 
  0x27, 0x02, 0xdb, 0xcc, 0xdc, 0xcb, 0xcd, 0xdb,
  0xdc, 0xdb, 0xcc, 0xcd, 0xdc, 0xcc, 0xdd, 0xdc, 
  0x27, 0x02, 0xcd, 0xdd, 0xdd, 0xec, 0xdd, 0xed,
  0xdd, 0xee, 0xdd, 0xdf, 0xdf, 0xfe, 0xee, 0xfe, 
  0x28, 0x03, 0xce, 0xde, 0xee, 0xfe, 0xee, 0xe0,
  0xf0, 0x0f, 0x00, 0x10, 0x00, 0x02, 0x12, 0x12, 
};

char err_msg[256];

// shits out an error message and locks up
void ERR_panic(char *msg) {
  RECT clearRect = { 0, 0, SCRW, SCRH*2 };
  DISPENV	disp;
  DRAWENV draw;
  int i, t;

  SpuSetKey(SPU_OFF, SPU_ALLCH);

  S_stopmusic();
  ResetCallback();
  ResetGraph(0);

  SetDefDispEnv(&disp, 0, 0, SCRW, SCRH);
  disp.isinter = disp.isrgb24 = 0;
  disp.screen.y = SCRY;
  disp.screen.h = SCRH;
  SetDefDrawEnv(&draw, 0, 0, SCRW, SCRH);

  PutDispEnv(&disp);
  PutDrawEnv(&draw);
  ClearImage(&clearRect, 80, 0, 0);
  DrawSync(0);

  SetDispMask(1);

  printf("FATAL ERROR: %s\n", msg);

  // prepare sound

  SpuSetTransferMode(SpuTransByDMA);
  SpuSetTransferStartAddr(SINADDR);
  SpuWrite((u_char *)sinewave, sizeof(sinewave));
  SpuIsTransferCompleted(SPU_TRANSFER_WAIT);

  // prepare channel

  chanattr.voice = SINCH;
  chanattr.mask = SPU_VOICE_VOLL | SPU_VOICE_VOLR | SPU_VOICE_WDSA |
    SPU_VOICE_PITCH;
  chanattr.addr = SINADDR;
  chanattr.pitch = 0x3FFF;
  chanattr.volume.left = chanattr.volume.right = 0x2FFF;
  SpuSetVoiceAttr(&chanattr);

  VSync(0);

  // do the beeps
  // have to do this because regular VSync(n) tends to time out and die
  #define VSYNCN(x) t = x; while (t-- > 0) VSync(0)
  while (1) {
    VSYNCN(WSP * 6);

    for (i = 0; i < 256 && msg[i]; ++i) {
      int c = toupper(msg[i]);
      int j;

      if (c >= '0' && c <= '9')
        c -= '0';
      else if (c >= 'A' && c <= 'Z')
        c = c - 'A' + 10;
      else if (c == '-')
        c = 'M' - 'A' + 10; // `M`inus
      else {
        VSYNCN(WSP);
        continue;
      }

      for (j = 0; j < 5 && morse[c][j]; ++j) {
        SpuSetKey(SPU_ON, SINCH);
        VSYNCN(morse[c][j]);
        SpuSetKey(SPU_OFF, SINCH);
        VSYNCN(DAH);
      }

      VSYNCN(LSP);
    }
  }
}

void ERR_close_all(void) {
  S_done();
  S_donemusic();
  K_done();
  mc_done();
  V_done();
  M_shutdown();
}

void ERR_quit(void) {
  void * p;

  // V_done();
  // if(!(p=malloc(4000)))
  puts("ERR_quit()\n");
  // else {
  //    F_loadres(F_getresid("ENDOOM"),p,0,4000);
  //  memcpy((void*)0xB8000,p,4000);free(p);gotoxy(1,24);
  // }
  ERR_close_all();
  exit(0);
}
