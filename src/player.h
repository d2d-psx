/*
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 *
 * This file is part of the Doom2D PSX project.
 *
 * Doom2D PSX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Doom2D PSX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

// Player data and functions

#define PL_DRAWLIFE     1
#define PL_DRAWARMOR    2
#define PL_DRAWWPN      4
#define PL_DRAWFRAG     8
#define PL_DRAWAIR      16
#define PL_DRAWKEYS     32

#define PLF_FIRE        1
#define PLF_PNSND       2
#define PLF_UP          4
#define PLF_DOWN        8

#define PL_POWERUP_TIME 546

enum {
  PLK_U, PLK_D, PLK_L, PLK_R ,
  PLK_F, PLK_J, PLK_WL, PLK_WR,
  PLK_P, PLK_S, PLK_H,
  PLK_NUMKEYS
};

#pragma pack(1)
typedef struct {
  obj_t o;
  int   looky;
  int   st, s;
  int   life, armor, hit, hito;
  int   pain, air;
  int   invl, suit;
  signed char  d;
  // byte ku,kd,kl,kr,kf,kj,kwl,kwr,kp;
  int   frag, ammo, shel, rock, cell, fuel, kills, secrets;
  byte  fire, cwpn, csnd, cidl;
  byte  amul;
  word  wpns;
  signed char  wpn;
  byte  f;
  byte  drawst;
  byte  color;
  int   id;
  byte  keys;
  int   ku, kd, kl, kr, kf, kj, kwl, kwr, kp, ks, kh;
} player_t;
#pragma pack()

extern const u_long plk_ofs[PLK_NUMKEYS];
extern const char *plk_names[PLK_NUMKEYS];

void PL_init(void);
void PL_alloc(void);
void PL_spawn(player_t *, int, int, signed char);
int PL_hit(player_t *, int, int, int);
int PL_isdead(player_t *);
void PL_act(player_t *);
void PL_draw(player_t *);
void PL_drawst(player_t *);
void PL_cry(player_t *);
void PL_damage(player_t *);
int PL_give(player_t *, int);
void G_respawn_player(player_t *);

void PL_reset(void);
void * PL_getspr(int c, int d);

extern byte p_immortal;
extern player_t pl1, pl2;
